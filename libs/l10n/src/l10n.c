#include "l10n.h"
#include <string.h>

l10n_translate_f *l10n_api(translate) = NULL;

#define msglang(code) static const char *_##code##_translate(l10n_hash_t hash) { (void)hash; return
#define endlang() NULL; }
#define msgent(id, str) ((hash) == l10n_hash(id)) ? (str) :

#include L10N_STRINGS_H

#undef msglang
#undef endlang
#undef msgent

int l10n_api(set_lang)(const char *lang) {
#define msglang(code) strstr(lang, #code) ? _##code##_translate :
#define msgent(id, str)
#define endlang() NULL

  l10n_api(translate) =
#include L10N_STRINGS_H
    ;
  
#undef msglang
#undef endlang
#undef msgent
  
  return l10n_api(translate) ? 1 : 0;
}
