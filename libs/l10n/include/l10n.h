#ifndef L10N_H
#define L10N_H

#ifndef USE_L10N
#define USE_L10N 1
#endif

#ifndef L10N_FMUL
#define L10N_FMUL 1
#endif

#ifndef L10N_HASH
#define L10N_HASH djb2
#endif

#if !USE_L10N

#define t(msg) msg
#define l10n_use(lang) 0

#else /* USE_L10N */

#include <stddef.h>
#include <stdint.h>

#ifndef L10N_INSTANCE
#define L10N_INSTANCE
#endif

#define l10n_api__(inst, name) l10n_##inst##_##name
#define l10n_api_(inst, name) l10n_api__(inst, name)
#define l10n_api(name) l10n_api_(L10N_INSTANCE, name)

#define t(msg) l10n_fallback(msg)
#define l10n_use(lang) l10n_api(set_lang)(lang)

#define l10n_fallback(msg) ({                                           \
      const char *lmsg = l10n_api(translate) ? l10n_api(translate)(l10n_hash(msg)) : NULL; \
      if (lmsg == NULL) lmsg = msg;                                     \
      lmsg;                                                             \
    })

typedef uint32_t l10n_hash_t;

#define _l10n_djb2_init 5381
#if L10N_FMUL
#define _l10n_djb2_calc(h, c) (h * 33 + c)
#else /* L10N_FMUL */
#define _l10n_djb2_calc(h, c) ((h << 5) + h + c)
#endif /* L10N_FMUL */

#define _l10n_sdbm_init 0
#if L10N_FMUL
#define _l10n_sdbm_calc(h, c) (h * 65599 + c)
#else /* L10N_FMUL */
#define _l10n_sdbm_calc(h, c) ((h << 6) + (h << 16) - h + c)
#endif /* L10N_FMUL */

#define _l10n_hash_(ht, op) _l10n_##ht##_##op
#define _l10n_hash(ht, op) _l10n_hash_(ht, op)

#ifdef __GNUC__
#define L10N_CONST __attribute__((const,always_inline))
#else
#define L10N_CONST
#endif

static inline L10N_CONST l10n_hash_t l10n_hash(const char *str) {
  l10n_hash_t val = _l10n_hash(L10N_HASH, init);
  
  for (; *str != '\0'; )
    val = _l10n_hash(L10N_HASH, calc)(val, *str++);
  
  return val;
}

typedef const char *l10n_translate_f(l10n_hash_t hash);

extern l10n_translate_f *l10n_api(translate);

int l10n_api(set_lang)(const char *lang);

#endif /* USE_L10N */

#endif /* L10N_H */
