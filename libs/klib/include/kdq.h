#ifndef __AC_KDQ_H
#define __AC_KDQ_H

#include <stdlib.h>
#include <string.h>

#ifndef kalloc
#define kalloc(Z) malloc(Z)
#endif

#ifndef krealloc
#define krealloc(P, Z) realloc(P, Z)
#endif

#ifndef kfree
#define kfree(P) free(P)
#endif

#define kdq_t(name) _kdq_t_(name)
#define _kdq_t_(name) kdq_##name##_t

#define kdq_size(q) ((q)->count)
#define kdq_first(q) ((q)->a[(q)->front])
#define kdq_last(q) ((q)->a[((q)->front + (q)->count - 1) & (q)->mask])
#define kdq_at(q, i) ((q)->a[((q)->front + (i)) & (q)->mask])

#define KDQ_DECL(name, scope, len_t, val_t) _KDQ_DECL_(name, scope, len_t, val_t)
#define _KDQ_DECL_(name, scope, len_t, val_t)                    \
  typedef struct {                                               \
    len_t front;                                                 \
    len_t bits;                                                  \
    len_t count;                                                 \
    len_t mask;                                                  \
    val_t *a;                                                    \
  } kdq_##name##_t;                                              \
  scope kdq_##name##_t *kdq_init_##name(void);                   \
  scope void kdq_prepare_##name(kdq_##name##_t *q);              \
  scope void kdq_finalize_##name(kdq_##name##_t *q);             \
  scope void kdq_destroy_##name(kdq_##name##_t *q);              \
  scope len_t kdq_resize_##name(kdq_##name##_t *q,               \
                                len_t new_bits);                 \
  scope val_t *kdq_pushp_##name(kdq_##name##_t *q);              \
  scope void kdq_push_##name(kdq_##name##_t *q, val_t v);        \
  scope val_t *kdq_unshiftp_##name(kdq_##name##_t *q);           \
  scope void kdq_unshift_##name(kdq_##name##_t *q, val_t v);     \
  scope val_t *kdq_pop_##name(kdq_##name##_t *q);                \
  scope val_t *kdq_shift_##name(kdq_##name##_t *q);

#define KDQ_IMPL(name, scope, len_t, val_t) _KDQ_IMPL_(name, scope, len_t, val_t)
#define _KDQ_IMPL_(name, scope, len_t, val_t)                    \
  scope kdq_##name##_t *kdq_init_##name(void) {                  \
    kdq_##name##_t *q =                                          \
      (kdq_##name##_t*)kalloc(sizeof(kdq_##name##_t));           \
    kdq_prepare_##name(q);                                       \
    return q;                                                    \
  }                                                              \
  scope void kdq_prepare_##name(kdq_##name##_t *q) {             \
    q->bits = 2, q->mask = (((len_t)1) << q->bits) - 1;          \
    q->front = q->count = 0;                                     \
    q->a = (val_t*)kalloc((((len_t)1) << q->bits) *              \
                          sizeof(val_t));                        \
  }                                                              \
  scope void kdq_finalize_##name(kdq_##name##_t *q) {            \
    kfree(q->a);                                                 \
  }                                                              \
  scope void kdq_destroy_##name(kdq_##name##_t *q) {             \
    if (q) {                                                     \
      kdq_finalize_##name(q);                                    \
      kfree(q);                                                  \
    }                                                            \
  }                                                              \
  scope len_t kdq_resize_##name(kdq_##name##_t *q,               \
                                len_t new_bits) {                \
    len_t new_size = ((len_t)1) << new_bits,                     \
      old_size = ((len_t)1) << q->bits;                          \
    if (new_size < q->count) { /* not big enough */              \
      len_t i;                                                   \
      for (i = 0; i < sizeof(len_t) * 8; ++i)                    \
        if (((len_t)1) << i > q->count) break;                   \
      new_bits = i, new_size = ((len_t)1) << new_bits;           \
    }                                                            \
    if (new_bits == q->bits) return q->bits; /* unchanged */     \
    if (new_bits > q->bits)                                      \
      q->a = (val_t*)krealloc(q->a, (((len_t)1) << new_bits) *   \
                              sizeof(val_t));                    \
    if (q->front + q->count <= old_size) { /* unwrapped */       \
      if (q->front + q->count > new_size)                        \
        /* only happens for shrinking */                         \
        memmove(q->a, q->a + new_size,                           \
                (q->front + q->count - new_size) *               \
                sizeof(val_t));                                  \
    } else { /* wrapped */                                       \
      memmove(q->a + (new_size - (old_size - q->front)),         \
              q->a + q->front, (old_size - q->front) *           \
              sizeof(val_t));                                    \
      q->front = new_size - (old_size - q->front);               \
    }                                                            \
    q->bits = new_bits, q->mask = (((len_t)1) << q->bits) - 1;   \
    if (new_bits < q->bits)                                      \
      q->a = (val_t*)krealloc(q->a, (((len_t)1) << new_bits) *   \
                             sizeof(val_t));                     \
    return q->bits;                                              \
  }                                                              \
  scope val_t *kdq_pushp_##name(kdq_##name##_t *q) {             \
    if (q->count == ((len_t)1) << q->bits)                       \
      kdq_resize_##name(q, q->bits + 1);                         \
    return &q->a[((q->count++) + q->front) & (q)->mask];         \
  }                                                              \
  scope void kdq_push_##name(kdq_##name##_t *q, val_t v) {       \
    if (q->count == ((len_t)1) << q->bits)                       \
      kdq_resize_##name(q, q->bits + 1);                         \
    q->a[((q->count++) + q->front) & (q)->mask] = v;             \
  }                                                              \
  scope val_t *kdq_unshiftp_##name(kdq_##name##_t *q) {          \
    if (q->count == ((len_t)1) << q->bits)                       \
      kdq_resize_##name(q, q->bits + 1);                         \
    ++q->count;                                                  \
    q->front = q->front ? q->front - 1 :                         \
      (((len_t)1) << q->bits) - 1;                               \
    return &q->a[q->front];                                      \
  }                                                              \
  scope void kdq_unshift_##name(kdq_##name##_t *q, val_t v) {    \
    val_t *p;                                                    \
    p = kdq_unshiftp_##name(q);                                  \
    *p = v;                                                      \
  }                                                              \
  scope val_t *kdq_pop_##name(kdq_##name##_t *q) {               \
    return q->count?                                             \
      &q->a[((--q->count) + q->front) & q->mask] : 0;            \
  }                                                              \
  scope val_t *kdq_shift_##name(kdq_##name##_t *q) {             \
    val_t *d = 0;                                                \
    if (q->count == 0) return 0;                                 \
    d = &q->a[q->front++];                                       \
    q->front &= q->mask;                                         \
    --q->count;                                                  \
    return d;                                                    \
  }

#ifndef klib_unused
#if (defined __clang__ && __clang_major__ >= 3) || (defined __GNUC__ && __GNUC__ >= 3)
#define klib_unused __attribute__ ((__unused__))
#else
#define klib_unused
#endif
#endif /* klib_unused */

#ifndef klib_global
#define klib_global extern
#endif

#ifndef klib_local
#define klib_local static inline klib_unused
#endif

#ifndef kdq_api
#define kdq_api(func, name) func##name
#endif

#define kdq_init(name) kdq_api(kdq_init_, name)

#define kdq_prepare(name, q) kdq_api(kdq_prepare_, name)(q)

#define kdq_finalize(name, q) kdq_api(kdq_finalize_, name)(q)

#define kdq_destroy(name, q) kdq_api(kdq_destroy_, name)(q)

#define kdq_resize(name, q, new_bits) kdq_api(kdq_resize_, name)(q, new_bits)

#define kdq_pushp(name, q) kdq_api(kdq_pushp_, name)(q)

#define kdq_push(name, q, v) kdq_api(kdq_push_, name)(q, v)

#define kdq_pop(name, q) kdq_api(kdq_pop_, name)(q)

#define kdq_unshiftp(name, q) kdq_api(kdq_unshiftp_, name)(q)

#define kdq_unshift(name, q, v) kdq_api(kdq_unshift_, name)(q, v)

#define kdq_shift(name, q) kdq_api(kdq_shift_, name)(q)

#endif
