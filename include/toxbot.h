#ifndef __TOXBOT_H__
#define __TOXBOT_H__

#include <tox/tox.h>
#include <uv.h>
#include <sodium.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <khash.h>
#include <kvec.h>
#include <klist.h>
#include <kdq.h>
#include <elastr.h>

#include <l10n.h>

#ifndef countof
#define countof(x) (sizeof(x)/sizeof(x[0]))
#endif

#ifndef offsetof
#define offsetof(type, member) ((uintptr_t)&((type*)0)->member)
#endif

#ifndef containerof
#define containerof(ptr, type, member) ((type*)((char*)(ptr) - offsetof(type, member)))
#endif

#define LOG_DEBUG 0x1
#define LOG_ERROR 0x2
#define LOG_STATE 0x4

#ifndef LOG_FLAGS
#define LOG_FLAGS LOG_DEBUG | LOG_ERROR | LOG_STATE
#endif

#ifdef TOXBOT_PLUGIN_NAME
#define LOG_MODULE_PLUGIN_(plugin) #plugin
#define LOG_MODULE_PLUGIN(plugin) LOG_MODULE_PLUGIN_(plugin)
#define LOG_MODULE(prefix) prefix "[" LOG_MODULE_PLUGIN(TOXBOT_PLUGIN_NAME) "]: "
#else
#define LOG_MODULE(prefix) prefix ": "
#endif

#if LOG_FLAGS & LOG_STATE
#define log(f, ...) fprintf(stdout, LOG_MODULE("") "" f "\n", ##__VA_ARGS__)
#else
#define log(f, ...)
#endif

#if LOG_FLAGS & LOG_ERROR
#define err(f, ...) fprintf(stderr, LOG_MODULE("ERROR") f "\n", ##__VA_ARGS__)
#else
#define err(f, ...)
#endif

#if LOG_FLAGS & LOG_DEBUG
#define dbg(f, ...) fprintf(stderr, LOG_MODULE("DEBUG") f "\n", ##__VA_ARGS__)
#else
#define dbg(f, ...)
#endif

#define new(t) (malloc(sizeof(t)))
#define alloc(s) (malloc(s))
#define del(p) (free(p))

typedef uint8_t toxbot_address_t[TOX_ADDRESS_SIZE];
typedef uint8_t toxbot_sha256_t[crypto_hash_sha256_BYTES];
typedef struct toxbot_s toxbot_t;

typedef struct {
  const char *version;
  const char *title;
  const char *description;
  const char *author;
  const char *location;
} toxbot_plugin_info_t;

typedef void toxbot_plugin_info_cb(toxbot_plugin_info_t *info);
typedef void toxbot_plugin_use_lang_cb(const char *lang, void *user_data);

#define toxbot_plugin_use_lang_func(name)               \
  static void name(const char *lang, void *user_data) { \
    (void)user_data;                                    \
    l10n_use(lang);                                     \
  }

typedef int toxbot_plugin_init_cb(void *user_data);
typedef void toxbot_plugin_done_cb(void *user_data);

typedef void toxbot_plugin_start_cb(Tox *tox, void *user_data);
typedef void toxbot_plugin_stop_cb(Tox *tox, void *user_data);

typedef struct {
  /* plugin callbacks */
  toxbot_plugin_info_cb *plugin_info;
  toxbot_plugin_use_lang_cb *plugin_use_lang;
  toxbot_plugin_init_cb *plugin_init;
  toxbot_plugin_done_cb *plugin_done;
  toxbot_plugin_start_cb *plugin_start;
  toxbot_plugin_stop_cb *plugin_stop;
  
  /* self callbacks */
  tox_self_connection_status_cb *self_connection_status;
  
  /* friend callbacks */
  tox_friend_connection_status_cb *friend_connection_status;
  tox_friend_name_cb *friend_name;
  tox_friend_status_cb *friend_status;
  tox_friend_status_message_cb *friend_status_message;
  tox_friend_typing_cb *friend_typing;
  tox_friend_read_receipt_cb *friend_read_receipt;
  tox_friend_request_cb *friend_request;
  tox_friend_message_cb *friend_message;
  /* extra */
  tox_friend_lossy_packet_cb *friend_lossy_packet;
  tox_friend_lossless_packet_cb *friend_lossless_packet;

  /* file callbacks */
  tox_file_recv_control_cb *file_recv_control;
  tox_file_chunk_request_cb *file_chunk_request;
  tox_file_recv_cb *file_recv;
  tox_file_recv_chunk_cb *file_recv_chunk;
  
  /* conference callbacks */
  tox_conference_invite_cb *conference_invite;
  tox_conference_title_cb *conference_title;
  tox_conference_message_cb *conference_message;
  tox_conference_namelist_change_cb *conference_namelist_change;
} toxbot_plugin_vmt_t;

typedef enum {
  toxbot_plugin_none,
  toxbot_plugin_native,
  toxbot_plugin_lua,
} toxbot_plugin_type_t;

typedef struct {
  char *name;
#if TOXBOT_PLUGIN_LOADABLE
  toxbot_plugin_type_t type;
  char *file;
  void *lib;
#endif
  const toxbot_plugin_vmt_t *vmt;
  toxbot_t *bot;
  void *data;
} toxbot_plugin_t;

KMAP_DECL(toxbot_plugins, klib_global, kh_cstr_t, toxbot_plugin_t);

#define toxbot_plugin_vmt_symbol__(prefix, plugin) prefix##plugin
#define toxbot_plugin_vmt_symbol_(prefix, plugin) toxbot_plugin_vmt_symbol__(prefix, plugin)
#define toxbot_plugin_vmt_symbol(plugin) toxbot_plugin_vmt_symbol_(TOXBOT_PLUGIN_TABLE_PREFIX, plugin)
#define toxbot_plugin_vmt const toxbot_plugin_vmt_t toxbot_plugin_vmt_symbol(TOXBOT_PLUGIN_NAME)

#if TOXBOT_PLUGIN_LOADABLE
#define toxbot_plugin_initial { NULL, toxbot_plugin_none, NULL, NULL, NULL, NULL, NULL }
#else
#define toxbot_plugin_initial { NULL, NULL, NULL }
#endif

typedef struct {
  TOX_MESSAGE_TYPE type;
  char *message;
  size_t length;
  uint32_t message_id;
} toxbot_msg_t;

KDQ_DECL(toxbot_msgs, klib_global, uint32_t, toxbot_msg_t);

typedef struct {
  toxbot_t *bot;
  uint32_t friend_number;
  kdq_t(toxbot_msgs) send_queue;
  uv_idle_t send_defer;
} toxbot_chat_t;

#if USE_DEBUG
KMAP_DECL(toxbot_chats, klib_global, khint32_t, toxbot_chat_t*);
#else
KMAP_DECL(toxbot_int_voidp, klib_global, khint32_t, void*);
#define toxbot_chats toxbot_int_voidp
#endif

toxbot_chat_t *toxbot_chat_get(toxbot_plugin_t *plugin, uint32_t friend_number);
void toxbot_chat_send(toxbot_chat_t *chat, TOX_MESSAGE_TYPE type,
                      const char *fmt, ...);

int cmd_split_arg(const char **cmd_line, const char *arg_str);

KSET_DECL(c_strings, klib_global, kh_cstr_t);

void parse_namelist(khash_t(c_strings) *h, const char *s);

#if !USE_DEBUG
KDQ_DECL(toxbot_voidp, klib_global, uint32_t, void*);
#endif

struct toxbot_s {
  const char *self_name;
  const char *profile_path;
  toxbot_sha256_t profile_hash;
#if TOXBOT_PROFILE_USE_MMAP
  size_t profile_size;
  void *profile_data;
#endif
  bool use_udp;
  bool use_ipv6;
  bool plugin_info;
  bool alive;
  const char *lang;
  Tox *tox;
#if TOXBOT_PLUGIN_LOADABLE
  const char *plugin_paths;
#endif
  khash_t(c_strings) whitelist;
  khash_t(c_strings) blacklist;
  khash_t(toxbot_plugins) plugins;
  uv_loop_t *loop;
  uv_timer_t iter_timer;
  uv_signal_t int_signal;
  khash_t(toxbot_chats) chats;
};

int toxbot_getopt(toxbot_t *bot, int argc, char *argv[]);
void toxbot_iterate(toxbot_t *bot);
void toxbot_bootstrap(toxbot_t *bot);
int toxbot_profile_load(toxbot_t *bot, struct Tox_Options *opts);
int toxbot_profile_save(toxbot_t *bot);
void toxbot_plugins_init(toxbot_t *bot);
void toxbot_plugins_done(toxbot_t *bot);
void toxbot_plugins_info(toxbot_t *bot);

void bin2hex(char *out, const uint8_t *ptr, uint32_t len);
int hex2bin(uint8_t *out, const char *ptr, uint32_t len);

#endif /* __TOXBOT_H__ */
