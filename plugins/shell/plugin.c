#include "toxbot.h"

static void
plugin_info_cb(toxbot_plugin_info_t *info) {
  info->version = TOXBOT_PROGRAM_VERSION_STR;
  info->title = t("System shell");
  info->description = t("This plugin allows you execute shell commands on host system");
  info->author = TOXBOT_PROGRAM_AUTHORS;
  info->location = TOXBOT_PROGRAM_LOCATION;
}

toxbot_plugin_use_lang_func(plugin_use_lang_cb);

typedef union {
  struct {
    uv_stdio_container_t container_stdin;
    uv_stdio_container_t container_stdout;
    uv_stdio_container_t container_stderr;
  };
  uv_stdio_container_t stdio[3];
} shell_stdio_containers_t;

typedef struct {
  uv_write_t write;
  uv_buf_t buf;
  uint32_t friend_number;
} input_t;

#if USE_DEBUG
KDQ_DECL(input, klib_local, uint32_t, input_t*);
KDQ_IMPL(input, klib_local, uint32_t, input_t*);
#else
#define input toxbot_voidp
#endif

typedef struct {
  uint32_t friend_number;
  uv_process_options_t options;
  uv_process_t process;
  shell_stdio_containers_t stdio;
  uv_pipe_t process_stdin;
  uv_pipe_t process_stdout;
  uv_pipe_t process_stderr;
  kdq_t(input) input;
} shell_chat_t;

#if USE_DEBUG
KMAP_DECL(plugin_chats, klib_local, khint32_t, shell_chat_t*);
KMAP_IMPL(plugin_chats, klib_local, khint32_t, shell_chat_t*, kh_int32_hash);
#else
#define plugin_chats toxbot_int_voidp
#endif

typedef struct {
  khash_t(plugin_chats) chats;
} plugin_t;

static int
plugin_init_cb(void * user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = new(plugin_t);
  
  kh_prepare(plugin_chats, &plugin->chats);
  
  handle->data = plugin;
  
  return 1;
}

static void
plugin_done_cb(void * user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;

  /* TODO: terminate running shells */

  kh_finalize(plugin_chats, &plugin->chats);
  del(plugin);
}

static void
shell_exit_cb(uv_process_t *process, int64_t exit_status, int term_signal) {
  (void)exit_status;
  (void)term_signal;
  toxbot_plugin_t *handle = process->data;
  plugin_t *plugin = handle->data;
  
  dbg("Shell process exit: %d", (int)exit_status);
  
  khint_t iter = kh_get(plugin_chats, &plugin->chats, process->pid);
  
  if (iter != kh_end(&plugin->chats)) {
    kh_del(plugin_chats, &plugin->chats, iter);
  }
}

static shell_chat_t *
shell_chat_find(plugin_t *plugin,
                uint32_t friend_number) {
  khiter_t iter = kh_get(plugin_chats, &plugin->chats, friend_number);

  if (iter == kh_end(&plugin->chats)) {
    return NULL;
  }

  return kh_value(&plugin->chats, iter);
}

#define MAX_DATA_LENGTH (TOX_MAX_MESSAGE_LENGTH - sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND) - 1)

static void
alloc_cb(uv_handle_t* handle,
         size_t suggested_size,
         uv_buf_t* buf) {
  (void)handle;
  
  buf->len = suggested_size > MAX_DATA_LENGTH ? MAX_DATA_LENGTH : suggested_size;
  buf->base = (char*)alloc(buf->len + sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND) + 1) + sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND);

  dbg("alloc_cb: suggested %u, allocated %u", (unsigned int)suggested_size, (unsigned int)buf->len);
}

static void
stdin_write_cb(uv_write_t* write_req,
               int status) {
  input_t *req = containerof(write_req, input_t, write);
  toxbot_plugin_t *handle = write_req->data;
  plugin_t *plugin = handle->data;
  shell_chat_t *shell_chat = shell_chat_find(plugin, req->friend_number);
  
  del(req->buf.base);

  if (status < 0) {
    err("Error when writing to stdin: %s",
        uv_strerror(status));
  }
  
  if (!shell_chat) {
    goto end;
  }

  if (status < 0) {
    toxbot_chat_t *chat = toxbot_chat_get(handle, req->friend_number);
    
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_ACTION,
                     t("Error when sending command to shell: %s"),
                     uv_strerror(status));
  }
  
  kdq_shift(input, &shell_chat->input);

 end:
  del(req);
}

static int
shell_chat_send(toxbot_chat_t *chat,
                ssize_t nread,
                const uv_buf_t* buf) {
  if (nread > 0) {
    memcpy(buf->base - sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND),
           TOXBOT_PLUGIN_SHELL_CHAT_COMMAND,
           sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND));
    
    *(char*)(buf->base - 1) = ' ';
    *(char*)(buf->base + nread) = '\0';
    
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_ACTION, buf->base - sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND));
  }

  if (buf->base && buf->len) {
    del(buf->base - sizeof(TOXBOT_PLUGIN_SHELL_CHAT_COMMAND));
  }

  return nread < 0 ? 0 : 1;
}

static void
stdout_read_cb(uv_stream_t* stream,
               ssize_t nread,
               const uv_buf_t* buf) {
  if (nread < 0) {
    err("Error when reading stdout: %s", uv_strerror(nread));
  }
  
  toxbot_plugin_t *handle = stream->data;
  shell_chat_t *shell_chat = containerof(stream, shell_chat_t, process_stdout);

  if (!shell_chat) {
    /* free data */
    shell_chat_send(NULL, 0, buf);
    return;
  }
  
  toxbot_chat_t *chat = toxbot_chat_get(handle, shell_chat->friend_number);
  
  if (!shell_chat_send(chat, nread, buf)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_ACTION,
                     t("Error when reading stdout: %s"),
                     uv_strerror(nread));
  }
}

static void
stderr_read_cb(uv_stream_t* stream,
               ssize_t nread,
               const uv_buf_t* buf) {
  if (nread < 0) {
    err("Error when reading stderr: %s", uv_strerror(nread));
  }
  
  toxbot_plugin_t *handle = stream->data;
  shell_chat_t *shell_chat = containerof(stream, shell_chat_t, process_stderr);

  if (!shell_chat) {
    /* free data */
    shell_chat_send(NULL, 0, buf);
    return;
  }
  
  toxbot_chat_t *chat = toxbot_chat_get(handle, shell_chat->friend_number);

  if (!shell_chat_send(chat, nread, buf)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_ACTION,
                     t("Error when reading stderr: %s"),
                     uv_strerror(nread));
  }
}

static shell_chat_t *
shell_chat_get(toxbot_plugin_t *handle,
               uint32_t friend_number) {
  plugin_t *plugin = handle->data;
  
  khint_t iter = kh_get(plugin_chats, &plugin->chats, friend_number);
  
  if (iter != kh_end(&plugin->chats)) {
  ret:
    dbg("Shell process started");
    return kh_val(&plugin->chats, iter);
  }

  int ret;
  shell_chat_t *shell_chat = new(shell_chat_t);
  
  shell_chat->friend_number = friend_number;
  
  uv_loop_t *loop = handle->bot->loop;
  uv_process_options_t *options = &shell_chat->options;
  shell_stdio_containers_t *stdio = &shell_chat->stdio;
  
  options->exit_cb = shell_exit_cb;
  options->args = NULL;
  options->env = NULL;
  options->cwd = NULL;
  options->flags = 0;
  options->stdio_count = countof(stdio->stdio);
  options->stdio = stdio->stdio;
  options->uid = 0;
  options->gid = 0;
  
  if (0 > (ret = uv_pipe_init(loop, &shell_chat->process_stdin, 0)) ||
      0 > (ret = uv_pipe_init(loop, &shell_chat->process_stdout, 0)) ||
      0 > (ret = uv_pipe_init(loop, &shell_chat->process_stderr, 0))) {
    goto err;
  }
  
  stdio->container_stdin.flags = UV_CREATE_PIPE | UV_READABLE_PIPE;
  stdio->container_stdin.data.stream = (uv_stream_t*)&shell_chat->process_stdin;
  stdio->container_stdout.flags = UV_CREATE_PIPE | UV_WRITABLE_PIPE;
  stdio->container_stdout.data.stream = (uv_stream_t*)&shell_chat->process_stdout;
  stdio->container_stderr.flags = UV_CREATE_PIPE | UV_WRITABLE_PIPE;
  stdio->container_stderr.data.stream = (uv_stream_t*)&shell_chat->process_stderr;
  
  shell_chat->process.data =
    shell_chat->process_stdin.data =
    shell_chat->process_stdout.data =
    shell_chat->process_stderr.data =
    handle;
  
  options->file = getenv(TOXBOT_PLUGIN_SHELL_EXEC_ENV_VAR);
  
  if (!options->file) {
    options->file = TOXBOT_PLUGIN_SHELL_EXEC_DEFAULT;
  }

  dbg("Starting shell process");
  
  if ((ret = uv_spawn(loop, &shell_chat->process, options))) {
    /* TODO: close on error */
    goto err;
  }
  
  uv_read_start((uv_stream_t*)&shell_chat->process_stdout, alloc_cb, stdout_read_cb);
  uv_read_start((uv_stream_t*)&shell_chat->process_stderr, alloc_cb, stderr_read_cb);
  
  kdq_prepare(input, &shell_chat->input);
  
  iter = kh_put(plugin_chats, &plugin->chats, friend_number, &ret);
  kh_val(&plugin->chats, iter) = shell_chat;
  
  goto ret;

 err:
  err("%s", uv_strerror(ret));
  del(shell_chat);
  
  return NULL;
}

static void
friend_message_cb(Tox *tox,
                  uint32_t friend_number,
                  TOX_MESSAGE_TYPE type,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  (void)tox;
  (void)type;
  (void)length;
  
  toxbot_plugin_t *handle = user_data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, friend_number);
  
  const char *cmd_line = message;
  const char *message_end = message + length;
  
  if (cmd_split_arg(&cmd_line, "help")) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("%s <command>[ <arguments>] --- execute shell command"),
                     TOXBOT_PLUGIN_SHELL_CHAT_COMMAND);
  } else if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_SHELL_CHAT_COMMAND)) {
    if (cmd_line[0] != '\0' && cmd_line[0] != ' ') {
      shell_chat_t *shell_chat = shell_chat_get(handle, friend_number);

      if (!shell_chat) {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error while starting shell"));
        return;
      }
      
      if (type == TOX_MESSAGE_TYPE_NORMAL) {
        /* write message to stdin */
        size_t len = message_end - cmd_line;
        char *str = alloc(len + 1);
        
        memcpy(str, cmd_line, len);
        str[len] = '\0';

        dbg("Send command to shell: %s", str);

        str[len] = '\n';
        
        input_t *req = new(input_t);
        
        req->write.data = handle;
        req->buf.len = len + 1;
        req->buf.base = str;
        
        kdq_push(input, &shell_chat->input, req);
        
        uv_write(&req->write, (uv_stream_t*)&shell_chat->process_stdin, &req->buf, 1, stdin_write_cb);
      } else {
        /* send signal to shell */
        if (cmd_split_arg(&cmd_line, "C-c")) {
          uv_process_kill(&shell_chat->process, SIGINT);
        } else if (cmd_split_arg(&cmd_line, "C-\\")) {
          uv_process_kill(&shell_chat->process, SIGQUIT);
        } else if (cmd_split_arg(&cmd_line, "C-z")) {
          uv_process_kill(&shell_chat->process, SIGTSTP);
        } else if (cmd_split_arg(&cmd_line, "C-d")) {
          //uv_close(&shell_chat->stdin);
        }
      }
    } else {
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Please specify shell command"));
    }
  }
}

toxbot_plugin_vmt = {
  .plugin_info = plugin_info_cb,
  .plugin_use_lang = plugin_use_lang_cb,
  
  .plugin_init = plugin_init_cb,
  .plugin_done = plugin_done_cb,
  
  .friend_message = friend_message_cb,
};
