#include "toxbot.h"

static void
plugin_info_cb(toxbot_plugin_info_t *info) {
  info->version = TOXBOT_PROGRAM_VERSION_STR;
  info->title = t("Friends management");
  info->description = t("This plugin allows you control friends of your bot");
  info->author = TOXBOT_PROGRAM_AUTHORS;
  info->location = TOXBOT_PROGRAM_LOCATION;
}

toxbot_plugin_use_lang_func(plugin_use_lang_cb);

static uint32_t friend_find(Tox *tox, const char *str) {
  size_t friends = tox_self_get_friend_list_size(tox);
  
  uint32_t friend_numbers[friends];
  uint32_t *friend_number = friend_numbers;
  uint32_t *friend_number_out = friend_numbers + friends;
  
  tox_self_get_friend_list(tox, friend_numbers);

  size_t len = strlen(str);

  toxbot_address_t key;
  
  if (len == TOX_PUBLIC_KEY_SIZE &&
      hex2bin(key, str, len)) {
    str = NULL;
  }
  
  for (; friend_number < friend_number_out; friend_number++) {
    if (str) {
      size_t size = tox_friend_get_name_size(tox, *friend_number, NULL);
      char name[size + 1];
      tox_friend_get_name(tox, *friend_number, name, NULL);
      name[size] = '\0';

      if (0 == strcmp(name, str)) {
        return *friend_number;
      }
    } else {
      toxbot_address_t toxid;
      tox_friend_get_public_key(tox, *friend_number, toxid, NULL);

      if (0 == memcmp(toxid, key, TOX_PUBLIC_KEY_SIZE)) {
        return *friend_number;
      }
    }
  }

  return UINT32_MAX;
}

static void
friend_message_cb(Tox *tox,
                  uint32_t friend_number,
                  TOX_MESSAGE_TYPE type,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  (void)type;

  const char *message_end = message + length;
  toxbot_plugin_t *handle = user_data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, friend_number);
  
  const char *cmd_line = message;

  if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_HELP_COMMAND)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("%s <list|add|del> --- friends list management"),
                     TOXBOT_PLUGIN_FRIEND_COMMAND);
  } else if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_FRIEND_COMMAND)) {
    if (cmd_split_arg(&cmd_line, "add")) {
      const char *ptr = strchr(cmd_line, ' ');
      uint8_t toxid_len = ptr ? ptr - cmd_line : message_end - cmd_line;
      toxbot_address_t toxid;
      
      hex2bin(toxid, cmd_line, toxid_len);
      
      const char *msg = NULL;
      uint8_t msg_len = 0;

      if (ptr) {
        msg = ptr + 1;
        msg_len = message_end - ptr + 1;
      } else {
        msg = t("Hello! I'm your smart bot. Add me to your roster, please.");
        msg_len = strlen(msg);
      }

      /* send request to friend */
      TOX_ERR_FRIEND_ADD err;
      tox_friend_add(tox, toxid, msg, msg_len, &err);

      if (err != TOX_ERR_FRIEND_ADD_OK) {
        err("friend add error: %u", err);
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when adding user"));
      } else {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("User added"));
      }
    } else if (cmd_split_arg(&cmd_line, "del")) {
      uint32_t found_friend_number = friend_find(tox, cmd_line);
      
      if (friend_number != UINT32_MAX) {
        TOX_ERR_FRIEND_DELETE err;
        tox_friend_delete(tox, found_friend_number, &err);
      
        if (err != TOX_ERR_FRIEND_DELETE_OK) {
          err("friend del error: %u", err);
          toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when deleting user"));
        } else {
          toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("User deleted"));
        }
      } else {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("User not found"));
      }
    } else if (cmd_split_arg(&cmd_line, "list")) {
      size_t friends = tox_self_get_friend_list_size(tox);
      
      if (friends > 0) {
        uint32_t friend_numbers[friends];
        uint32_t *friend_number_ptr = friend_numbers;
        uint32_t *friend_number_end = friend_numbers + friends;
        
        tox_self_get_friend_list(tox, friend_numbers);
        
        for (; friend_number_ptr < friend_number_end; friend_number_ptr++) {
          size_t name_size = tox_friend_get_name_size(tox, *friend_number_ptr, NULL);
          char entry[TOX_PUBLIC_KEY_SIZE*2 + 1 + name_size + 1];
          
          toxbot_address_t toxid;
          tox_friend_get_public_key(tox, *friend_number_ptr, toxid, NULL);
          bin2hex(entry, toxid, TOX_PUBLIC_KEY_SIZE);
          entry[TOX_PUBLIC_KEY_SIZE*2] = ' ';
          
          tox_friend_get_name(tox, *friend_number_ptr, &entry[TOX_PUBLIC_KEY_SIZE*2 + 1], NULL);
          entry[TOX_PUBLIC_KEY_SIZE*2 + 1 + name_size] = '\0';
          toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, entry);
        }
      }
    } else {
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Please specify sub-command:"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("list --- list known trusted friends"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("add <toxid>[ <message>] --- add new friend"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("del <name|toxid|pubkey> --- delete friend"));
    }
    return;
  }
}

toxbot_plugin_vmt = {
  .plugin_info = plugin_info_cb,
  .plugin_use_lang = plugin_use_lang_cb,
  
  .friend_message = friend_message_cb,
};
