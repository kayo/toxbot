#include "toxbot.h"

typedef struct {
  toxbot_address_t *master_id;
} plugin_t;

static void
plugin_info_cb(toxbot_plugin_info_t *info) {
  info->version = TOXBOT_PROGRAM_VERSION_STR;
  info->title = t("Master request");
  info->description = t("This plugin makes the bot capable to send request to its owner to add to your friends list");
  info->author = TOXBOT_PROGRAM_AUTHORS;
  info->location = TOXBOT_PROGRAM_LOCATION;
}

toxbot_plugin_use_lang_func(plugin_use_lang_cb);

static int
plugin_init_cb(void *user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = new(plugin_t);
  
  plugin->master_id = NULL;
  
  const char *master_toxid = getenv(TOXBOT_MASTER_TOXID_ENV_VAR);
  
  if (master_toxid) {
    toxbot_address_t master_address;
    
    if (hex2bin(master_address, master_toxid, strlen(master_toxid))) {
      plugin->master_id = new(toxbot_address_t);
      memcpy(*plugin->master_id, master_address, sizeof(master_address));
    } else {
      err("Invalid master ToxID `%s`", master_toxid);
    }
  } else {
    err("Please set master ToxID env var TOXBOT_MASTER_TOXID.");
  }
  
  handle->data = plugin;
  
  return 1;
}

static void
plugin_done_cb(void *user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  
  if (plugin->master_id) {
    del(plugin->master_id);
  }
  
  del(plugin);
}

static void
self_connection_status_cb(Tox *tox,
                          TOX_CONNECTION connection_status,
                          void *user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  
  switch (connection_status) {
  case TOX_CONNECTION_NONE:
    break;
  case TOX_CONNECTION_TCP:
  case TOX_CONNECTION_UDP:
    if (plugin->master_id && UINT32_MAX == tox_friend_by_public_key(tox, *plugin->master_id, NULL)) {
      {
        char hexstr[sizeof(*plugin->master_id)*2+1];
        bin2hex(hexstr, *plugin->master_id, sizeof(*plugin->master_id));
        hexstr[sizeof(*plugin->master_id)*2] = '\0';
        log("add master to friends: %s", hexstr);
      }
      
      /* send request to master */
      const char *msg = t("Hello! I'm your smart bot. Add me to your roster, please.");
      
      TOX_ERR_FRIEND_ADD err;
      tox_friend_add(tox, *plugin->master_id, msg, strlen(msg), &err);
      
      if (err != TOX_ERR_FRIEND_ADD_OK) {
        err("friend add error: %u", err);
      }
    }
    
    break;
  }
}

toxbot_plugin_vmt = {
  .plugin_info = plugin_info_cb,
  .plugin_use_lang = plugin_use_lang_cb,
  
  .plugin_init = plugin_init_cb,
  .plugin_done = plugin_done_cb,
  
  .self_connection_status = self_connection_status_cb,
};
