#include "toxbot.h"

#include <unistd.h>

static void
plugin_info_cb(toxbot_plugin_info_t *info) {
  info->version = TOXBOT_PROGRAM_VERSION_STR;
  info->title = t("File transfer");
  info->description = t("This plugin allows you exchange files with remote system");
  info->author = TOXBOT_PROGRAM_AUTHORS;
  info->location = TOXBOT_PROGRAM_LOCATION;
}

toxbot_plugin_use_lang_func(plugin_use_lang_cb);

typedef struct transfer_req transfer_req_t;

struct transfer_req {
  uv_fs_t req;
  uint32_t friend_number;
  uint32_t file_number;
  uint64_t pos;
  uv_buf_t buf;
  bool complete;
};

#if USE_DEBUG
KDQ_DECL(transfer_reqs, klib_local, uint32_t, transfer_req_t*);
KDQ_IMPL(transfer_reqs, klib_local, uint32_t, transfer_req_t*);
#else
#define transfer_reqs toxbot_voidp
#endif

static transfer_req_t *
transfer_req_new(toxbot_plugin_t *handle,
                 uint32_t friend_number,
                 uint32_t file_number,
                 uint64_t position,
                 size_t length) {
  transfer_req_t *transfer_req = new(transfer_req_t);

  transfer_req->friend_number = friend_number;
  transfer_req->file_number = file_number;
  transfer_req->pos = position;
  transfer_req->buf.len = length;
  transfer_req->buf.base = alloc(length);
  transfer_req->req.data = handle;
  transfer_req->complete = false;
  
  return transfer_req;
}

static void
transfer_req_cleanup(transfer_req_t *transfer_req) {
  if (transfer_req->buf.base) {
    del(transfer_req->buf.base);
    transfer_req->buf.base = NULL;
  }
}

static void
transfer_req_del(transfer_req_t *transfer_req) {
  del(transfer_req);
}

typedef struct {
  uv_fs_t req;
  uint32_t friend_number;
  uint32_t file_number;
  int fd;
  size_t size;
  kdq_t(transfer_reqs) reqs;
} file_transfer_t;

#if USE_DEBUG
KMAP_DECL(file_transfers, klib_local, khint32_t, file_transfer_t*);
KMAP_IMPL(file_transfers, klib_local, khint32_t, file_transfer_t*, kh_int32_hash);
#else
#define file_transfers toxbot_int_voidp
#endif

typedef struct {
  uint32_t friend_number;
  char *current_dir;
  khash_t(file_transfers) transfers;
} file_chat_t;

#if USE_DEBUG
KMAP_DECL(file_chats, klib_local, khint32_t, file_chat_t*);
KMAP_IMPL(file_chats, klib_local, khint32_t, file_chat_t*, kh_int32_hash);
#else
#define file_chats toxbot_int_voidp
#endif

static file_transfer_t *
file_transfer_get(file_chat_t *file_chat,
                  uint32_t file_number) {
  khiter_t iter = kh_get(file_transfers, &file_chat->transfers,
                         file_number);
  
  return iter != kh_end(&file_chat->transfers) ?
    kh_val(&file_chat->transfers, iter) : NULL;
}

static file_transfer_t *
file_transfer_new(file_chat_t *file_chat,
                  uint32_t file_number,
                  toxbot_plugin_t *handle,
                  char *file_path,
                  size_t file_size) {
  khiter_t iter = kh_get(file_transfers, &file_chat->transfers,
                         file_number);
  
  if (iter != kh_end(&file_chat->transfers)) {
    return NULL;
  }
  
  file_transfer_t *transfer = new(file_transfer_t);

  dbg("file_transfer_new: %u", file_number);
  
  transfer->friend_number = file_chat->friend_number;
  transfer->file_number = file_number;
  transfer->req.data = handle;
  transfer->req.path = file_path;
  transfer->fd = -1;
  transfer->size = file_size;
  kdq_prepare(transfer_reqs, &transfer->reqs);

  int ret;
  iter = kh_put(file_transfers, &file_chat->transfers, file_number, &ret);
  kh_val(&file_chat->transfers, iter) = transfer;
  
  return transfer;
}

static void
close_cb(uv_fs_t *req);

static void
file_transfer_del(file_chat_t *file_chat,
                  uint32_t file_number) {
  khiter_t iter = kh_get(file_transfers, &file_chat->transfers,
                         file_number);

  if (iter != kh_end(&file_chat->transfers)) {
    file_transfer_t *transfer = kh_val(&file_chat->transfers, iter);
    toxbot_plugin_t *handle = transfer->req.data;

    if (transfer->fd < 0) {
      dbg("file_transfer_del: %u", file_number);
      kh_del(file_transfers, &file_chat->transfers, iter);
      del(transfer);
      
      return;
    }
    
    uv_fs_close(handle->bot->loop, &transfer->req, transfer->fd, close_cb);
  }
}

typedef struct {
  khash_t(file_chats) chats;
} plugin_t;

static int
plugin_init_cb(void *user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = new(plugin_t);
  
  kh_prepare(file_chats, &plugin->chats);
  
  handle->data = plugin;
  
  return 1;
}

static void
file_transfers_rm(file_chat_t *file_chat) {
  khiter_t iter = kh_begin(&file_chat->transfers);
  for (; iter < kh_end(&file_chat->transfers); iter++) {
    if (kh_exist(&file_chat->transfers, iter)) {
      file_transfer_t *transfer = kh_val(&file_chat->transfers, iter);
      
      kh_del(file_transfers, &file_chat->transfers, iter);
      del(transfer);
    }
  }
}

static void
file_chats_rm(plugin_t *plugin) {
  khiter_t iter = kh_begin(&plugin->chats);
  for (; iter < kh_end(&plugin->chats); iter++) {
    if (kh_exist(&plugin->chats, iter)) {
      file_chat_t *file_chat = kh_val(&plugin->chats, iter);
      
      kh_del(file_chats, &plugin->chats, iter);

      file_transfers_rm(file_chat);
      del(file_chat);
    }
  }
}

static void
plugin_done_cb(void *user_data) {
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  
  file_chats_rm(plugin);
  
  del(plugin);
}

static file_chat_t *
file_chat_get(plugin_t *plugin,
              uint32_t friend_number) {
  khiter_t iter = kh_get(file_chats, &plugin->chats, friend_number);
  
  if (iter != kh_end(&plugin->chats)) {
  ret:
    return kh_val(&plugin->chats, iter);
  }
  
  file_chat_t *file_chat = new(file_chat_t);
  
  file_chat->friend_number = friend_number;
  
#ifdef TOXBOT_PLUGIN_FILE_PATH_ENV_VAR
  file_chat->current_dir = getenv(TOXBOT_PLUGIN_FILE_PATH_ENV_VAR);
  
  if (!file_chat->current_dir) {
#endif
    file_chat->current_dir = get_current_dir_name();
#ifdef TOXBOT_PLUGIN_FILE_PATH_ENV_VAR
  }
#endif
  
  kh_prepare(file_transfers, &file_chat->transfers);

  int ret;
  iter = kh_put(file_chats, &plugin->chats, friend_number, &ret);
  kh_val(&plugin->chats, iter) = file_chat;
  
  goto ret;
}

typedef struct {
  uv_fs_t req;
  uint32_t friend_number;
} file_req_t;

static file_req_t *
file_req_new(toxbot_plugin_t *handle,
             uint32_t friend_number) {
  file_req_t *file_req = new(file_req_t);
  
  file_req->friend_number = friend_number;
  file_req->req.data = handle;
  
  return file_req;
}

static void
file_req_use(file_req_t *file_req) {
  uv_fs_req_cleanup(&file_req->req);
}

static void
file_req_del(file_req_t *file_req) {
  uv_fs_req_cleanup(&file_req->req);
  del(file_req);
}

static void statdir_cb(uv_fs_t* req) {
  file_req_t *file_req = containerof(req, file_req_t, req);
  toxbot_plugin_t *handle = req->data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, file_req->friend_number);

  if (req->result < 0) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Error when checking object: %s"),
                     uv_strerror(req->result));
    goto end;
  }
  
  const char *file = strrchr(req->path, '/');

  if (file) {
    file ++;
  } else {
    file = req->path;
  }
  
  toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, "  %s%s %zu",
                   file,
                   req->statbuf.st_mode & S_IFDIR ? "/" : "",
                   (size_t)req->statbuf.st_size);
  
 end:
  file_req_del(file_req);
}

static void scandir_cb(uv_fs_t* req) {
  file_req_t *file_req = containerof(req, file_req_t, req);
  
  if (req->result < 0) {
    toxbot_plugin_t *handle = req->data;
    toxbot_chat_t *chat = toxbot_chat_get(handle, file_req->friend_number);
    
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Error when scanning directory: %s"),
                     uv_strerror(req->result));
    goto end;
  }
  
  toxbot_plugin_t *handle = req->data;
  uv_dirent_t ent;
  
  for (; UV_EOF != uv_fs_scandir_next(req, &ent); ) {
    estr_t path;
    estr_heap_init(&path, NULL);
    
    estr_putsr(&path, req->path);
    estr_putc(&path, '/');
    estr_putsr(&path, ent.name);
    
    file_req_t *stat_req = file_req_new(handle, file_req->friend_number);
    
    uv_fs_stat(handle->bot->loop, &stat_req->req, estr_str(&path), statdir_cb);
  }

 end:
  file_req_del(file_req);
}

static void chdir_cb(uv_fs_t* req) {
  file_req_t *file_req = containerof(req, file_req_t, req);
  toxbot_plugin_t *handle = req->data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, file_req->friend_number);

  if (req->result < 0) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Error when checking object: %s"),
                     uv_strerror(req->result));
    goto end;
  }
  
  if (!(req->statbuf.st_mode & S_IFDIR)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Target path isn't a directory"));
    goto end;
  }

  /* is a directory */
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, file_req->friend_number);
  
  del(file_chat->current_dir);
  file_chat->current_dir = (char*)req->path;

  toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                   t("Directory changed to: %s"),
                   file_chat->current_dir);
  req->path = NULL; /* prevent free path */
  goto end;

 end:
  file_req_del(file_req);
}

static void
get_open_cb(uv_fs_t *req) {
  file_req_t *file_req = containerof(req, file_req_t, req);
  toxbot_plugin_t *handle = req->data;
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, file_req->friend_number);
  char *path = (char*)req->path;
  
  if (req->result < 0) {
    toxbot_chat_t *chat = toxbot_chat_get(handle, file_req->friend_number);
    
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when opening file for read"));
    err("Error when opening file for read: %s", uv_strerror(req->result));
    goto end;
  }

  const char *name = strrchr(path, '/');

  if (name) {
    name++;
  } else {
    name = req->path;
  }
  
  uint32_t file_number = tox_file_send(handle->bot->tox, file_req->friend_number,
                                       TOX_FILE_KIND_DATA, req->statbuf.st_size,
                                       NULL, name, strlen(name), NULL);
  
  file_transfer_t *transfer = file_transfer_new(file_chat, file_number, handle,
                                                NULL, req->statbuf.st_size);
  
  transfer->fd = req->result;
  dbg("get_open_cb");

 end:
  file_req_del(file_req);
}

static void get_stat_cb(uv_fs_t* req) {
  file_req_t *file_req = containerof(req, file_req_t, req);
  toxbot_plugin_t *handle = req->data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, file_req->friend_number);
  char *path = (char*)req->path;

  if (req->result < 0) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Error when checking object: %s"),
                     uv_strerror(req->result));
    goto err;
  }
  
  if (!(req->statbuf.st_mode & S_IFREG)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Target path isn't a regular file"));
    goto err;
  }

  if (req->statbuf.st_size == 0) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("Target file is empty"));
    goto err;
  }
  
  /* is a regular file */
  req->path = NULL; /* prevent free path */
  file_req_use(file_req);
  uv_fs_open(handle->bot->loop, &file_req->req, path, O_RDONLY, 0, get_open_cb);
  
  return;
 
 err:
  file_req_del(file_req);
}

static void
friend_message_cb(Tox *tox,
                  uint32_t friend_number,
                  TOX_MESSAGE_TYPE type,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  (void)tox;
  (void)type;
  (void)length;

  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, friend_number);
  const char *message_end = (const char*)message + length;
  
  const char *cmd_line = message;

  if (cmd_split_arg(&cmd_line, "help")) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("%s <command> --- file transfer"),
                     TOXBOT_PLUGIN_FILE_COMMAND);
  } else if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_FILE_COMMAND)) {
    if (cmd_split_arg(&cmd_line, "ls")) {
      /* show directory */
      file_chat_t *file_chat = file_chat_get(plugin, friend_number);
      estr_t path;
      estr_heap_init(&path, NULL);

      if (message_end - cmd_line == 0 || cmd_line[0] != '/') {
        estr_putsr(&path, file_chat->current_dir);
        estr_putc(&path, '/');
      }
      estr_putsn(&path, cmd_line, message_end - cmd_line);
      
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                       t("Scan directory: %s"),
                       estr_str(&path));
      
      file_req_t *file_req = file_req_new(handle, friend_number);
      
      uv_fs_scandir(handle->bot->loop, &file_req->req,
                    estr_str(&path), 0, scandir_cb);
    } else if (cmd_split_arg(&cmd_line, "cd")) {
      /* change current directory */
      if (cmd_line == message_end) {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                         t("Please specify path"));
        return;
      }

      file_chat_t *file_chat = file_chat_get(plugin, friend_number);
      estr_t path;
      estr_heap_init(&path, NULL);

      if (message_end - cmd_line == 0 || cmd_line[0] != '/') {
        estr_putsr(&path, file_chat->current_dir);
        estr_putc(&path, '/');
      }
      estr_putsn(&path, cmd_line, message_end - cmd_line);
      
      file_req_t *file_req = file_req_new(handle, friend_number);
      
      uv_fs_stat(handle->bot->loop, &file_req->req, estr_str(&path), chdir_cb);
    } else if (cmd_split_arg(&cmd_line, "get")) {
      /* download file */
      if (cmd_line == message_end) {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                         t("Please specify path"));
        return;
      }
      
      file_chat_t *file_chat = file_chat_get(plugin, friend_number);
      estr_t path;
      estr_heap_init(&path, NULL);

      if (message_end - cmd_line > 0 && cmd_line[0] != '/') {
        estr_putsr(&path, file_chat->current_dir);
        estr_putc(&path, '/');
      }
      estr_putsn(&path, cmd_line, message_end - cmd_line);
      
      file_req_t *file_req = file_req_new(handle, friend_number);
      
      uv_fs_stat(handle->bot->loop, &file_req->req, estr_str(&path), get_stat_cb);
    } else {
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Please specify sub-command:"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("ls[ <path>] --- show current directory"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("cd <path> --- change directory"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("get <file> --- download file"));
    }
    return;
  }
}

static void
get_read_cb(uv_fs_t *req) {
  toxbot_plugin_t *handle = req->data;
  plugin_t *plugin = handle->data;
  transfer_req_t *transfer_req = containerof(req, transfer_req_t, req);
  file_chat_t *file_chat = file_chat_get(plugin, transfer_req->friend_number);

  uv_fs_req_cleanup(req);

  if (!file_chat) {
    err("no file chat for: %u", transfer_req->friend_number);
    transfer_req_cleanup(transfer_req);
    transfer_req_del(transfer_req);
    return;
  }

  file_transfer_t *transfer = file_transfer_get(file_chat, transfer_req->file_number);

  if (!transfer || !kdq_size(&transfer->reqs)) {
    err("no file transfer for: %u", transfer_req->file_number);
    goto end;
  }
  
  if (transfer_req != kdq_first(&transfer->reqs)) {
    /* libtoxcore file transfer is chunk order sensitive o_O */
    transfer_req->complete = true;
    return;
  }

 send:
  if (transfer_req->req.result < 0) {
    err("Error while reading file: %s", uv_strerror(transfer_req->req.result));
    goto end;
  }
  
  dbg("file_send_chunk: file_number: %u, position: %u, length: %u", transfer_req->file_number, (unsigned int)transfer_req->pos, (unsigned int)transfer_req->buf.len);

  bool last;
  TOX_ERR_FILE_SEND_CHUNK error;
  
  if (!tox_file_send_chunk(handle->bot->tox, transfer_req->friend_number, transfer_req->file_number,
                           transfer_req->pos, transfer_req->buf.base, transfer_req->buf.len, &error)) {
    err("Error when sending chunk: %u", error);
  }

 end:
  last = transfer_req->pos + transfer_req->buf.len == transfer->size;
  
  kdq_shift(transfer_reqs, &transfer->reqs);
  transfer_req_cleanup(transfer_req);
  transfer_req_del(transfer_req);
  
  if (kdq_size(&transfer->reqs)) {
    transfer_req = kdq_first(&transfer->reqs);
    if (transfer_req && transfer_req->complete) {
      goto send;
    }
  } else if (last) {
    /* complete transfer */
    file_transfer_del(file_chat, transfer->file_number);
  }
}

static void
file_chunk_request_cb(Tox *tox,
                      uint32_t friend_number,
                      uint32_t file_number,
                      uint64_t position,
                      size_t length,
                      void *user_data) {
  (void)tox;
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, friend_number);
  file_transfer_t *transfer = file_transfer_get(file_chat, file_number);

  dbg("file_chunk_request_cb: file_number: %u, position: %u, length: %u", file_number, (unsigned int)position, (unsigned int)length);

  if (!length) {
    return;
  }
  
  transfer_req_t *transfer_req = transfer_req_new(handle,
                                                  friend_number, file_number,
                                                  position, length);
  
  kdq_push(transfer_reqs, &transfer->reqs, transfer_req);
  
  uv_fs_read(handle->bot->loop, &transfer_req->req, transfer->fd,
             &transfer_req->buf, 1, transfer_req->pos, get_read_cb);
}

static void
put_write_cb(uv_fs_t *req) {
  toxbot_plugin_t *handle = req->data;
  plugin_t *plugin = handle->data;
  transfer_req_t *transfer_req = containerof(req, transfer_req_t, req);
  file_chat_t *file_chat = file_chat_get(plugin, transfer_req->friend_number);
  
  uv_fs_req_cleanup(req);
  transfer_req_cleanup(transfer_req);
  
  if (!file_chat) {
    err("no file chat for: %u", transfer_req->friend_number);
  end0:
    transfer_req_del(transfer_req);
    return;
  }

  file_transfer_t *transfer = file_transfer_get(file_chat, transfer_req->file_number);
  
  if (!transfer || !kdq_size(&transfer->reqs)) {
    err("no file transfer for: %u", transfer_req->file_number);
    goto end0;
  }
  
  if (transfer_req != kdq_first(&transfer->reqs)) {
    /* libtoxcore file transfer is chunk order sensitive o_O */
    transfer_req->complete = true;
    return;
  }

 beg:
  if (transfer_req->req.result < 0) {
    err("Error while writing file: %s", uv_strerror(transfer_req->req.result));
  }

  bool last = transfer_req->pos + transfer_req->buf.len == transfer->size;
    
  kdq_shift(transfer_reqs, &transfer->reqs);
  transfer_req_del(transfer_req);
  
  if (kdq_size(&transfer->reqs)) {
    transfer_req = kdq_first(&transfer->reqs);
    if (transfer_req->complete) {
      goto beg;
    }
  } else if (last) {
    /* finalize file transfer */
    file_transfer_del(file_chat, transfer->file_number);
    return;
  }
}

static void
put_open_cb(uv_fs_t *req) {
  toxbot_plugin_t *handle = req->data;
  plugin_t *plugin = handle->data;
  file_transfer_t *transfer = containerof(req, file_transfer_t, req);
  file_chat_t *file_chat = file_chat_get(plugin, transfer->friend_number);
  toxbot_chat_t *chat = toxbot_chat_get(handle, transfer->friend_number);

  uv_fs_req_cleanup(req);
  
  if (req->result < 0) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when opening file for write"));
    tox_file_control(handle->bot->tox, transfer->friend_number, transfer->file_number, TOX_FILE_CONTROL_CANCEL, NULL);
    err("Error when opening file wor write: %s", uv_strerror(req->result));
    goto err;
  }
  
  if (!tox_file_control(handle->bot->tox, transfer->friend_number, transfer->file_number, TOX_FILE_CONTROL_RESUME, NULL)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when starting file upload"));
    goto err;
  }
  
  transfer->fd = req->result;
  
  dbg("put_open_cb");
  return;

 err:
  file_transfer_del(file_chat, transfer->file_number);
}

static void
file_recv_cb(Tox *tox,
             uint32_t friend_number,
             uint32_t file_number,
             uint32_t kind,
             uint64_t file_size,
             const uint8_t *filename,
             size_t filename_length,
             void *user_data) {
  dbg("file_recv_cb: kind: %u, file_number: %u, filename: %s, size: %u", kind, file_number, filename, (unsigned int)file_size);
  
  if (kind != TOX_FILE_KIND_DATA) {
    /* we accept data only, but reject avatars and etc. */
    tox_file_control(tox, friend_number, file_number, TOX_FILE_CONTROL_CANCEL, NULL);
    return;
  }
  
  toxbot_plugin_t *handle = user_data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, friend_number);

  if (!filename || !filename_length) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Attempt to upload file without name"));
    tox_file_control(tox, friend_number, file_number, TOX_FILE_CONTROL_CANCEL, NULL);
    return;
  }
  
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, friend_number);

  estr_t path;
  estr_heap_init(&path, NULL);
  estr_putsr(&path, file_chat->current_dir);
  estr_putc(&path, '/');
  estr_putsn(&path, filename, filename_length);
  
  dbg("file to write: %s", estr_str(&path));
  
  file_transfer_t *transfer = file_transfer_new(file_chat, file_number, handle, estr_str(&path), file_size);
  
  /* need open file first */
  uv_fs_open(handle->bot->loop, &transfer->req, estr_str(&path), O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR, put_open_cb);
}

static void
file_recv_chunk_cb(Tox *tox,
                   uint32_t friend_number,
                   uint32_t file_number,
                   uint64_t position,
                   const uint8_t *data,
                   size_t length,
                   void *user_data) {
  (void)tox;
  
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, friend_number);
  
  dbg("file_recv_chunk_cb: file_number: %u, position: %u, length: %u", file_number, (unsigned int)position, (unsigned int)length);
  
  file_transfer_t *transfer = file_transfer_get(file_chat, file_number);
  transfer_req_t *transfer_req = transfer_req_new(handle,
                                                  friend_number, file_number,
                                                  position, length);
  
  memcpy(transfer_req->buf.base, data, length);

  kdq_push(transfer_reqs, &transfer->reqs, transfer_req);
  
  uv_fs_write(handle->bot->loop, &transfer_req->req, transfer->fd,
              &transfer_req->buf, 1, position, put_write_cb);
}

static void
file_recv_control_cb(Tox *tox,
                     uint32_t friend_number,
                     uint32_t file_number,
                     TOX_FILE_CONTROL control,
                     void *user_data) {
  (void)tox;
  toxbot_plugin_t *handle = user_data;
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, friend_number);
  //file_transfer_t *transfer = file_transfer_get(file_chat, file_number);
  
  switch (control) {
  case TOX_FILE_CONTROL_PAUSE:
    dbg("file_recv_control_cb: pause");
    break;
  case TOX_FILE_CONTROL_RESUME:
    dbg("file_recv_control_cb: resume");
    //tox_file_control(tox, friend_number, file_number, TOX_FILE_CONTROL_RESUME, NULL);
    break;
  case TOX_FILE_CONTROL_CANCEL:
    dbg("file_recv_control_cb: cancel");
    file_transfer_del(file_chat, file_number);
    break;
  }
}

static void
close_cb(uv_fs_t *req) {
  toxbot_plugin_t *handle = req->data;
  file_transfer_t *transfer = containerof(req, file_transfer_t, req);
  plugin_t *plugin = handle->data;
  file_chat_t *file_chat = file_chat_get(plugin, transfer->friend_number);
  
  if (req->result < 0) {
    err("Error when closing file: %s", uv_strerror(req->result));
  }

  transfer->fd = -1;
  uv_fs_req_cleanup(req);
  file_transfer_del(file_chat, transfer->file_number);
}

toxbot_plugin_vmt = {
  .plugin_info = plugin_info_cb,
  .plugin_use_lang = plugin_use_lang_cb,
  
  .plugin_init = plugin_init_cb,
  .plugin_done = plugin_done_cb,
  
  .friend_message = friend_message_cb,

  .file_chunk_request = file_chunk_request_cb,
  .file_recv = file_recv_cb,
  .file_recv_chunk = file_recv_chunk_cb,
  .file_recv_control = file_recv_control_cb,
};
