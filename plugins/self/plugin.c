#include "toxbot.h"

static void
plugin_info_cb(toxbot_plugin_info_t *info) {
  info->version = TOXBOT_PROGRAM_VERSION_STR;
  info->title = t("Self control");
  info->description = t("This plugin allows you change name and status of your bot");
  info->author = TOXBOT_PROGRAM_AUTHORS;
  info->location = TOXBOT_PROGRAM_LOCATION;
}

toxbot_plugin_use_lang_func(plugin_use_lang_cb);

static void
friend_message_cb(Tox *tox,
                  uint32_t friend_number,
                  TOX_MESSAGE_TYPE type,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  (void)type;

  const char *message_end = message + length;
  toxbot_plugin_t *handle = user_data;
  toxbot_chat_t *chat = toxbot_chat_get(handle, friend_number);
  
  const char *cmd_line = message;

  if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_HELP_COMMAND)) {
    toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL,
                     t("%s <name|status> --- bot name and status management"),
                     TOXBOT_PLUGIN_SELF_COMMAND);
  } else if (cmd_split_arg(&cmd_line, TOXBOT_PLUGIN_SELF_COMMAND)) {
    if (cmd_split_arg(&cmd_line, "name")) {
      if (tox_self_set_name(tox, cmd_line, message_end - cmd_line, NULL)) {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Name changed"));
      } else {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when change name"));
      }
    } else if (cmd_split_arg(&cmd_line, "status")) {
      TOX_USER_STATUS status;
      if (cmd_split_arg(&cmd_line, "none")) {
        status = TOX_USER_STATUS_NONE;
      } else if (cmd_split_arg(&cmd_line, "away")) {
        status = TOX_USER_STATUS_AWAY;
      } else if (cmd_split_arg(&cmd_line, "busy")) {
        status = TOX_USER_STATUS_BUSY;
      } else {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Please specify valid status:"));
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("none, busy or away"));
        return;
      }
      tox_self_set_status(tox, status);
      size_t msg_len = message_end - cmd_line;
      if (msg_len > 0) {
        if (tox_self_set_status_message(tox, cmd_line, msg_len, NULL)) {
          toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Status and message changed"));
        } else {
          toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Error when change status message"));
        }
      } else {
        toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Status changed"));
      }
    } else {
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("Please specify sub-command:"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("name <new_name> --- change bot name"));
      toxbot_chat_send(chat, TOX_MESSAGE_TYPE_NORMAL, t("status <new_status>[ <new_message>] --- change status"));
    }
    return;
  }
}

toxbot_plugin_vmt = {
  .plugin_info = plugin_info_cb,
  .plugin_use_lang = plugin_use_lang_cb,
  
  .friend_message = friend_message_cb,
};
