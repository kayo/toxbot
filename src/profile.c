#include "toxbot.h"

#if TOXBOT_PROFILE_USE_MMAP
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

int toxbot_profile_load(toxbot_t *bot, struct Tox_Options *opts) { /* load profile */
  int res = 0;

  {
    struct stat s;
    
    if (0 == stat(bot->profile_path, &s) &&
        (s.st_mode & S_IFREG) &&
        s.st_size > 0) {
      res = 1;
      bot->profile_size = s.st_size;
    } else {
      bot->profile_size = 1;
    }
  }
  
  int fd = open(bot->profile_path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  
  if (-1 == fd) {
    err("unable to open profile `%s`", bot->profile_path);
    bot->profile_data = NULL;
    return 0;
  }
  
  bot->profile_data = mmap(NULL, bot->profile_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  
  if (res != 0) {
    tox_options_set_savedata_type(opts, TOX_SAVEDATA_TYPE_TOX_SAVE);
    tox_options_set_savedata_data(opts, bot->profile_data, bot->profile_size);
    
    dbg("profile loaded");
  }
  
  return res;
}

int toxbot_profile_save(toxbot_t *bot) { /* save profile */
  uint32_t size = tox_get_savedata_size(bot->tox);

  if (!size) {
    return 0;
  }
  
  if (size != bot->profile_size) {
    bot->profile_data = mremap(bot->profile_data, bot->profile_size, size, MREMAP_MAYMOVE);
    bot->profile_size = size;
  }
  tox_get_savedata(bot->tox, bot->profile_data);
  
  {
    toxbot_sha256_t hash;
    crypto_hash_sha256(hash, bot->profile_data, bot->profile_size);
    
    if (0 == memcmp(bot->profile_hash, hash, sizeof(hash))) {
      return 0;
    }
    
    memcpy(bot->profile_hash, hash, sizeof(hash));
  }
  
  dbg("profile saved");
  
  return 1;
}

#else /* TOXBOT_PROFILE_USE_MMAP */

int toxbot_profile_load(toxbot_t *bot, struct Tox_Options *opts) { /* load profile */
  size_t size;
  
  {
    struct stat s;
    
    if (0 != stat(bot->profile_path, &s) ||
        !(s.st_mode & S_IFREG) ||
        0 == s.st_size) {
      goto err;
    }
    
    size = s.st_size;
  }
  
  FILE *f = fopen(bot->profile_path, "r");
  
  if (!f) {
    err("unable to open profile `%s`", bot->profile_path);
    goto err;
  }
  
  uint8_t *data = malloc(size);
  fread(data, size, 1, f);
  
  tox_options_set_savedata_type(opts, TOX_SAVEDATA_TYPE_TOX_SAVE);
  tox_options_set_savedata_data(opts, data, size);
  
  fclose(f);
  
  crypto_hash_sha256(bot->profile_hash, data, size);
  
  dbg("profile loaded");
  
  return 1;

 err:
  memset(bot->profile_hash, 0, sizeof(bot->profile_hash));
  
  return 0;
}

int toxbot_profile_save(toxbot_t *bot) { /* save profile */
  uint32_t size = tox_get_savedata_size(bot->tox);

  if (!size) {
    return 0;
  }
  
  uint8_t data[size];
  
  tox_get_savedata(bot->tox, data);
  
  {
    toxbot_sha256_t hash;
    crypto_hash_sha256(hash, data, size);
    
    if (0 == memcmp(bot->profile_hash, hash, sizeof(hash))) {
      return 0;
    }
    
    memcpy(bot->profile_hash, hash, sizeof(hash));
  }
  
  FILE *f = fopen(bot->profile_path, "wb");
  
  fwrite(data, size, 1, f);
  
  fclose(f);
  
  dbg("profile saved");
  
  return 1;
}

#endif /* TOXBOT_PROFILE_USE_MMAP */
