#include "toxbot.h"
#include "tox_bootstrap.h"

/* bootstrap to dht with bootstrap_nodes */
void toxbot_bootstrap(toxbot_t *bot) {
  static unsigned int j = 0;
  
  if (j == 0)
    j = rand();
  
  int i = 0;
  while (i < 4) {
    struct bootstrap_node *d = &bootstrap_nodes[j % countof(bootstrap_nodes)];
    tox_bootstrap(bot->tox, d->address, d->port, d->key, 0);
    tox_add_tcp_relay(bot->tox, d->address, d->port, d->key, 0);
    i++;
    j++;
  }
}
