#include "toxbot.h"

static inline char
b2h(int8_t b) {
  return b + (b <= 9 ? '0' : 'A' - 10);
}

void
bin2hex(char *out,
        const uint8_t *ptr,
        uint32_t len) {
  const uint8_t *end = ptr + len;
  
  for (; ptr < end; ptr++) {
    *out++ = b2h((*ptr) >> 4);
    *out++ = b2h(*ptr & 0xf);
  }
}

static inline int8_t
h2b(char c) {
  return '0' <= c && c <= '9' ? c - '0' : 'a' <= c && c <= 'f' ? c - ('a' - 10) : 'A' <= c && c <= 'F' ? c - ('A' - 10) : -1;
}

int
hex2bin(uint8_t *out,
        const char *ptr,
        uint32_t len) {
  if (len % 2)
    return 0;
  
  const char *end = ptr + len;
  int8_t h1, h2;
  
  for (; ptr < end; ) {
    if ((h1 = h2b(*ptr++)) < 0)
      return 0;
    if ((h2 = h2b(*ptr++)) < 0)
      return 0;
    *out++ = (h1 << 4) | h2;
  }

  return 1;
}

int cmd_split_arg(const char **cmd_line, const char *arg_str) {
  const char *cmd_line_ptr = *cmd_line;
  const char *arg_str_ptr = arg_str;

  for ( ; *arg_str_ptr != '\0';
        cmd_line_ptr++, arg_str_ptr++) {
    if (*cmd_line_ptr != *arg_str_ptr) {
      return 0;
    }
    if (*cmd_line_ptr == ' ' && *cmd_line_ptr == '\0') {
      return 0;
    }
  }
 
  for (; *cmd_line_ptr == ' '; cmd_line_ptr++);
  
  *cmd_line = cmd_line_ptr;
  
  return 1;
}

KSET_IMPL(c_strings, klib_global, kh_cstr_t, kh_str_hash);

void parse_namelist(khash_t(c_strings) *h, const char *s) {
  const char *p = s;

  for (; ; p++) {
    if (*p == '\0' ||
        *p == ' ' ||
        *p == ':' ||
        *p == ';' ||
        *p == ',') {
      size_t l = p - s;
      if (l > 0) {
        char *k = alloc(l + 1);
        memcpy(k, s, l);
        k[l] = '\0';
        
        khiter_t i = kh_get(c_strings, h, k);
        if (i == kh_end(h)) { /* add string */
          int r;
          kh_put(c_strings, h, k, &r);
        } else { /* already have key */
          del(k);
        }
      }
      if (*p == '\0') {
        break;
      } else {
        s = p + 1;
      }
    }
  }
}
