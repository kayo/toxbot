#include "toxbot.h"

#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#if TOXBOT_PLUGIN_LOADABLE
static void plugin_loadable_done(toxbot_t *bot, toxbot_plugin_t *plugin);
#endif /* TOXBOT_PLUGIN_LOADABLE */

static void plugin_done(toxbot_t *bot, toxbot_plugin_t *plugin) {
  dbg("plugin done `%s`", plugin->name);
  
  if (plugin->vmt->plugin_done) {
    plugin->vmt->plugin_done(plugin);
  }

  khint_t iter = kh_get(toxbot_plugins, &bot->plugins, plugin->name);

#if TOXBOT_PLUGIN_LOADABLE
  plugin_loadable_done(bot, plugin);
#endif /* TOXBOT_PLUGIN_LOADABLE */
  
  kh_del(toxbot_plugins, &bot->plugins, iter);
}

static int plugin_init(toxbot_t *bot, toxbot_plugin_t *plugin) {
  dbg("plugin init `%s`", plugin->name);
  
  if (plugin->vmt->plugin_init) {
    if (!plugin->vmt->plugin_init(plugin)) {
#if TOXBOT_PLUGIN_LOADABLE
      if (plugin->file) {
        err("Unable to init plugin `%s` from `%s`", plugin->name, plugin->file);
      } else {
#endif /* TOXBOT_PLUGIN_LOADABLE */
        err("Unable to init plugin `%s`", plugin->name);
#if TOXBOT_PLUGIN_LOADABLE
      }
#endif /* TOXBOT_PLUGIN_LOADABLE */
      plugin_done(bot, plugin);
      return 0;
    }
  }
  
  int ret;
  khiter_t iter = kh_put(toxbot_plugins, &bot->plugins, plugin->name, &ret);
  kh_value(&bot->plugins, iter) = *plugin;
  
  return 1;
}

#if TOXBOT_PLUGIN_LOADABLE

static inline int native_plugin_init(toxbot_t *bot, toxbot_plugin_t *plugin) {
  (void)bot;
  uv_lib_t uv_lib;
  
  if (0 != uv_dlopen(plugin->file, &uv_lib)) {
    err("Unable to load plugin from `%s`. Error: %s", plugin->file, uv_dlerror(&uv_lib));
    return 0;
  }
  
  size_t len = strlen(plugin->name);
  char name[sizeof(TOXBOT_PLUGIN_TABLE_PREFIX_STR) + len];
  
  memcpy(name, TOXBOT_PLUGIN_TABLE_PREFIX_STR, sizeof(TOXBOT_PLUGIN_TABLE_PREFIX_STR)-1);
  memcpy(name + sizeof(TOXBOT_PLUGIN_TABLE_PREFIX_STR)-1, plugin->name, len);
  name[sizeof(TOXBOT_PLUGIN_TABLE_PREFIX_STR)-1 + len] = '\0';
  
  if (0 != uv_dlsym(&uv_lib, name, (void**)&plugin->vmt)) {
    err("Unable to find symbol `%s` in file `%s`. Error: %s", name, plugin->file, uv_dlerror(&uv_lib));
    return 0;
  }
  
  plugin->lib = new(uv_lib_t);
  *(uv_lib_t*)plugin->lib = uv_lib;
  
  return 1;
}

static inline void native_plugin_done(toxbot_t *bot, toxbot_plugin_t *plugin) {
  (void)bot;
  
  if (plugin->lib) {
    uv_dlclose(plugin->lib);
    del(plugin->lib);
  }
}

#if TOXBOT_PLUGIN_LOADABLE_LUA
static const toxbot_plugin_vmt_t lua_plugin_vmt;

static inline int lua_plugin_init(toxbot_t *bot, toxbot_plugin_t *plugin) {
  (void)bot;
  /* TODO */
  
  plugin->vmt = &lua_plugin_vmt;
  
  return 0;
}

static inline void lua_plugin_done(toxbot_t *bot, toxbot_plugin_t *plugin) {
  (void)bot;
  (void)plugin;
  /* TODO */
}
#endif /* TOXBOT_PLUGIN_LOADABLE_LUA */

static int plugin_loadable_init(toxbot_t *bot, toxbot_plugin_t *plugin) {
  switch (plugin->type) {
  case toxbot_plugin_native:
    dbg("load native plugin `%s` from path `%s`", plugin->name, plugin->file);
    return native_plugin_init(bot, plugin);
#if TOXBOT_PLUGIN_LOADABLE_LUA
  case toxbot_plugin_lua:
    dbg("load lua plugin `%s` from file `%s`", plugin->name, plugin->file);
    return lua_plugin_init(bot, plugin);
#endif /* TOXBOT_PLUGIN_LOADABLE_LUA */
  default:
    break;
  }
  return 0;
}

static bool
plugin_enabled(toxbot_t *bot,
               const char *name) {
  if (0 < kh_size(&bot->whitelist) &&
      kh_get(c_strings, &bot->whitelist, name) ==
      kh_end(&bot->whitelist)) {
    return false;
  }

  if (0 < kh_size(&bot->blacklist) &&
      kh_get(c_strings, &bot->blacklist, name) !=
      kh_end(&bot->blacklist)) {
    return false;
  }

  return true;
}

static inline int plugins_init_from_file(toxbot_t *bot, const char *path, size_t path_len, const char* node) {
  if (node[0] == '.' &&
      (node[1] == '\0' ||
       (node[1] == '.' &&
        node[2] == '\0'))) {
    return 0;
  }
  
  toxbot_plugin_t plugin = toxbot_plugin_initial;
  
  plugin.bot = bot;
  
  size_t file_len = strlen(node);
  const char *file_end = node + file_len;
  
  /* determine plugin type */
  plugin.type =
    0 == memcmp(file_end - 3, ".so", 3) ? toxbot_plugin_native :
#if TOXBOT_PLUGIN_LOADABLE_LUA
    0 == memcmp(file_end - 4, ".lua", 4) ||
    0 == memcmp(file_end - 5, ".luac", 5) ? toxbot_plugin_lua :
#endif /* TOXBOT_PLUGIN_LOADABLE_LUA */
    toxbot_plugin_none;
  
  if (plugin.type == toxbot_plugin_none) {
    return 0;
  }
  
  /* make path to plugin file */
  char file[path_len + 1 + file_len + 1];
  
  memcpy(file, path, path_len);
  file[path_len] = '/';
  memcpy(file + path_len + 1, node, file_len + 1);
  file_len += path_len + 1;
  
  { /* check plugin file */
    struct stat st;
    if (0 != stat(file, &st) ||
        !(st.st_mode & S_IFREG) ||
        0 == st.st_size) {
      return 0;
    }
  }
  
  /* make name of plugin */
  const char *str = strrchr(file, '/');
  const char *end = strrchr(file, '.');
  
  if (str) {
    str ++; /* skip slash */
  } else {
    str = file; /* set to begining of path */
  }
  
  /* length of name */
  size_t name_len = end - str;
  
  char name[name_len];
  memcpy(name, str, name_len);
  name[name_len] = '\0';
  
  if (!plugin_enabled(bot, name) ||
      kh_get(toxbot_plugins, &bot->plugins, name)
      != kh_end(&bot->plugins)) {
    return 0;
  }
  
  /* temporary set plugin name and file path */
  plugin.name = name;
  plugin.file = file;
  
  if (!plugin_loadable_init(bot, &plugin)) {
    return 0;
  }
  
  /* permanently set plugin name and file path */
  plugin.name = alloc(name_len + 1);
  memcpy(plugin.name, name, name_len + 1);
  plugin.file = alloc(file_len + 1);
  memcpy(plugin.file, file, file_len + 1);
  
  if (!plugin_init(bot, &plugin)) {
    return 0;
  }
  
  dbg("loaded plugin `%s` from file `%s`", plugin.name, plugin.file);
  
  return 1;
}

static inline void plugins_init_from_path(toxbot_t *bot, const char* path) {
  DIR *dir = opendir(path);

  if (!dir) {
    err("Plugin path `%s` not exists", path);
    return;
  }
  
  size_t path_len = strlen(path);
  struct dirent *ent;

  for (; (ent = readdir(dir)); ) {
    plugins_init_from_file(bot, path, path_len, ent->d_name);
  }

  closedir(dir);
}

static void plugin_loadable_done(toxbot_t *bot, toxbot_plugin_t *plugin) {
  switch (plugin->type) {
  case toxbot_plugin_native:
    native_plugin_done(bot, plugin);
    break;
#if TOXBOT_PLUGIN_LOADABLE_LUA
  case toxbot_plugin_lua:
    lua_plugin_done(bot, plugin);
    break;
#endif /* TOXBOT_PLUGIN_LOADABLE_LUA */
  default:
    break;
  }
  
  if (plugin->file) {
    del(plugin->name);
    del(plugin->file);
  }
}

#endif /* TOXBOT_PLUGIN_LOADABLE */

#define _(name) extern const toxbot_plugin_vmt_t toxbot_plugin_vmt_symbol(name);
TOXBOT_PLUGIN_EMBEDDED
#undef _

void toxbot_plugins_init(toxbot_t *bot) {
#define _(NAME)                            \
  if (plugin_enabled(bot, #NAME)) {        \
    khint_t iter =                         \
      kh_get(toxbot_plugins,               \
             &bot->plugins, #NAME);        \
    if (iter == kh_end(&bot->plugins)) {   \
      toxbot_plugin_t plugin =             \
        toxbot_plugin_initial;             \
      plugin.name = #NAME;                 \
      plugin.vmt =                         \
        &toxbot_plugin_vmt_symbol(NAME);   \
      plugin.bot = bot;                    \
      plugin_init(bot, &plugin);           \
    }                                      \
  }
  TOXBOT_PLUGIN_EMBEDDED
#undef _
  
#if TOXBOT_PLUGIN_LOADABLE
  const char *paths = bot->plugin_paths;
  
#ifdef TOXBOT_PLUGIN_PATH_ENV_VAR
  if (!paths) {
    paths = getenv(TOXBOT_PLUGIN_PATH_ENV_VAR);
  }
#endif

#ifdef TOXBOT_PLUGIN_PATH_DEFAULT
  if (!paths) {
    paths = TOXBOT_PLUGIN_PATH_DEFAULT;
  }
#endif

  if (!paths) {
    return;
  }

  char dirs[strlen(paths)+1];
  strcpy(dirs, paths);
  
  char *dir = dirs, *end;
  
  for (; ; ) {
    end = strchr(dir, ':');
    
    if (end) {
      *end = '\0';
    }
    
    if ((end && dir < end) || 0 < strlen(dir)) {
      plugins_init_from_path(bot, dir);
    }
    
    if (end) {
      dir = end + 1;
    } else {
      break;
    }
  }
#endif /* TOXBOT_PLUGIN_LOADABLE */
}

void toxbot_plugins_done(toxbot_t *bot) {
  khint_t iter = kh_begin(&bot->plugins);

  for (; iter != kh_end(&bot->plugins); iter++) {
    if (kh_exist(&bot->plugins, iter)) {
      toxbot_plugin_t *plugin =
        &kh_value(&bot->plugins, iter);

      plugin_done(bot, plugin);
    }
  }
}

static inline void
plugin_info(toxbot_plugin_t *plugin) {
  log("%s:", plugin->name);
  log("  file: %s", plugin->file);
  
  if (!plugin->vmt->plugin_info) {
    goto show_callbacks;
  }

  toxbot_plugin_info_t info;

  plugin->vmt->plugin_info(&info);

#define log_field(field)                       \
  if (info.field) {                            \
    log("  " #field ": %s", info.field);       \
  }

  log_field(version);
  log_field(title);
  log_field(description);
  log_field(author);
  log_field(location);

 show_callbacks:
  log("  callback:");
  
#define log_method(method)     \
  if (plugin->vmt->method) {   \
    log("    - " #method);     \
  }

  log_method(plugin_info);
  log_method(plugin_use_lang);
  log_method(plugin_init);
  log_method(plugin_done);
  log_method(plugin_start);
  log_method(plugin_stop);

  log_method(self_connection_status);

  log_method(friend_connection_status);
  log_method(friend_name);
  log_method(friend_status);
  log_method(friend_status_message);
  log_method(friend_typing);
  log_method(friend_read_receipt);
  log_method(friend_request);
  log_method(friend_message);
  log_method(friend_lossy_packet);
  log_method(friend_lossless_packet);

  log_method(file_recv_control);
  log_method(file_chunk_request);
  log_method(file_recv);
  log_method(file_recv_chunk);

  log_method(conference_invite);
  log_method(conference_title);
  log_method(conference_message);
  log_method(conference_namelist_change);
}

void toxbot_plugins_info(toxbot_t *bot) {
  khint_t iter = kh_begin(&bot->plugins);

  for (; iter != kh_end(&bot->plugins); iter++) {
    if (kh_exist(&bot->plugins, iter)) {
      toxbot_plugin_t *plugin =
        &kh_value(&bot->plugins, iter);

      plugin_info(plugin);
    }
  }
}
