#include "toxbot.h"

#include <getopt.h>
#include <string.h>

static inline void
show_help(void) {
  log("Usage: " TOXBOT_PROGRAM_NAME_STR " [options]");
  log("  -h|--help  Show this help");
  log("  -v|--version  Show version info");
  log("  -i|--plugin-info  Show plugin info");
  log("  -w|--white-list=<plugins>  Enabled plugins");
  log("  -b|--black-list=<plugins>  Disabled plugins");
  log("  -n|--self-name=<name>  Self name");
  log("  -p|--profile-path=<path>  Path to profile");
  log("  -l|--l10n-lang=<lang>  Locale language");
  log("  -t|--no-udp  Disable UDP");
  log("  -4|--no-ipv6  Disable IPv6");
}

static inline void
show_version(void) {
  log(TOXBOT_PROGRAM_NAME_STR " " TOXBOT_PROGRAM_VERSION_STR);
}

static const char *langs[] = {
#define _(lang) #lang,
  TOXBOT_PROGRAM_LANGUAGE
  TOXBOT_PROGRAM_LANGUAGES
#undef _
};

int toxbot_getopt(toxbot_t *bot, int argc, char *argv[]) {
  for (; ;) {
    int option_index = 0;
    const char *short_options = "hviw:b:n:p:m:l:t4";
    static struct option long_options[] = {
      {"help",         no_argument,       NULL, 'h' },
      {"version",      no_argument,       NULL, 'v' },
      {"plugin-info",  no_argument,       NULL, 'i' },
      {"white-list",   required_argument, NULL, 'w' },
      {"black-list",   required_argument, NULL, 'b' },
      {"self-name",    required_argument, NULL, 'n' },
      {"profile-path", required_argument, NULL, 'p' },
      {"l10n-lang",    required_argument, NULL, 'l' },
      {"no-udp",       no_argument,       NULL, 't' },
      {"no-ipv6",      no_argument,       NULL, '4' },
      {0,              0,                 NULL, 0   }
    };
    
    char c = getopt_long(argc, argv, short_options,
                         long_options, &option_index);
    
    if (c == -1)
      break;
    
    switch (c) {
    case 'h':
      show_help();
      return 0;
    case 'v':
      show_version();
      return 0;
    case 'i':
      bot->plugin_info = true;
      break;
    case 'w':
      parse_namelist(&bot->whitelist, optarg);
      break;
    case 'b':
      parse_namelist(&bot->blacklist, optarg);
      break;
    case 'n':
      if (strlen(optarg) > tox_max_name_length()) {
        err("Self name too long");
        return 0;
      }
      bot->self_name = optarg;
      break;
    case 'p':
      bot->profile_path = optarg;
      break;
    case 'l':
      {
        const char **lang_ptr = langs, **lang_end = langs + countof(langs);
        for (; lang_ptr < lang_end; lang_ptr++) {
          if (strstr(*lang_ptr, optarg)) {
            bot->lang = optarg;
            break;
          }
        }
        if (lang_ptr == lang_end) {
          err("Unsupported localization. Please, use one of:");
          lang_ptr = langs, lang_end = langs + countof(langs);
          for (; lang_ptr < lang_end; lang_ptr++) {
            err("%s", *lang_ptr);
          }
          return 0;
        }
      }
      break;
    case 't':
      bot->use_udp = false;
      break;
    case '4':
      bot->use_ipv6 = false;
      break;
    }
  }
  
  return 1;
}
