#include "toxbot.h"

#include <string.h>
#include <stdarg.h>

static toxbot_chat_t *
toxbot_chat_find(toxbot_t *bot, uint32_t friend_number) {
  khint_t iter = kh_get(toxbot_chats, &bot->chats, friend_number);
  
  if (iter == kh_end(&bot->chats)) {
    return NULL;
  }

  return kh_value(&bot->chats, iter);
}

static void toxbot_chat_deque(toxbot_chat_t *chat) {
 beg:
  if (0 == kdq_size(&chat->send_queue)) {
    return;
  }
  
  toxbot_msg_t *msg = &kdq_first(&chat->send_queue);
  
  if (msg->message == NULL) {
    kdq_shift(toxbot_msgs, &chat->send_queue);
    goto beg;
  }
}

static void toxbot_chat_resume(toxbot_chat_t *chat) {
 beg:
  if (0 == kdq_size(&chat->send_queue)) {
    return;
  }

  uint32_t iter = 0;
  toxbot_msg_t *msg;
  
  for (; ; iter++) {
    if (iter == kdq_size(&chat->send_queue)) {
      return;
    }
    
    msg = &kdq_at(&chat->send_queue, iter);
    
    if (msg->message && !msg->message_id) {
      break;
    }
  }
  
  TOX_ERR_FRIEND_SEND_MESSAGE err;
  uint32_t message_id = tox_friend_send_message(chat->bot->tox, chat->friend_number, msg->type, msg->message, msg->length, &err);

  dbg("sent msg %u", msg->message_id);
  
  if (err == TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_CONNECTED ||
      err == TOX_ERR_FRIEND_SEND_MESSAGE_SENDQ) {
    return;
  }
  
  if (err == TOX_ERR_FRIEND_SEND_MESSAGE_OK) {
    msg->message_id = message_id;
  } else {
    err("friend send error: %u", err);

    del(msg->message);
    msg->message = NULL;
  }
  
  goto beg;
}

static void toxbot_chat_resume_defer_cb(uv_idle_t* handle) {
  toxbot_chat_t *chat = containerof(handle, toxbot_chat_t, send_defer);
  
  uv_idle_stop(handle);
  toxbot_chat_resume(chat);
}

static void toxbot_chat_receipt(toxbot_t *bot, uint32_t friend_number, uint32_t message_id) {
  toxbot_chat_t *chat = toxbot_chat_find(bot, friend_number);
  
  if (chat) {
    uint32_t iter = 0;
    for (; iter < kdq_size(&chat->send_queue); iter++) {
      toxbot_msg_t *msg = &kdq_at(&chat->send_queue, iter);

      if (msg->message_id == message_id) {
        del(msg->message);
        msg->message = NULL;
      }
    }
    
    toxbot_chat_deque(chat);
    
    uv_idle_start(&chat->send_defer, toxbot_chat_resume_defer_cb);
  }
}

static void toxbot_chat_close_empty(toxbot_t *bot, uint32_t friend_number) {
  toxbot_chat_t *chat = toxbot_chat_find(bot, friend_number);

  if (chat && 0 == kdq_size(&chat->send_queue)) {
    dbg("remove chat for %u", friend_number);
    khint_t iter = kh_get(toxbot_chats, &bot->chats, friend_number);
    
    del(kh_val(&bot->chats, iter));
    kh_del(toxbot_chats, &bot->chats, iter);
  }
}

toxbot_chat_t *toxbot_chat_get(toxbot_plugin_t *plugin, uint32_t friend_number) {
  toxbot_t *bot = plugin->bot;
  khiter_t iter = kh_get(toxbot_chats, &bot->chats, friend_number);
  toxbot_chat_t *chat;
  
  if (iter == kh_end(&bot->chats)) {
    dbg("create chat for %u", friend_number);
    int ret;
    iter = kh_put(toxbot_chats, &bot->chats, friend_number, &ret);
    chat = kh_value(&bot->chats, iter) = new(toxbot_chat_t);

    chat->bot = bot;
    chat->friend_number = friend_number;

    uv_idle_init(plugin->bot->loop, &chat->send_defer);
    
    kdq_prepare(toxbot_msgs, &chat->send_queue);
  } else {
    chat = kh_value(&bot->chats, iter);
  }
  
  return chat;
}

static void toxbot_msgs_rm(toxbot_chat_t *chat) {
  uint32_t iter = 0;
  
  for (; iter < kdq_size(&chat->send_queue); iter++) {
    toxbot_msg_t *msg = &kdq_at(&chat->send_queue, iter);
    
    if (msg->message) {
      del(msg->message);
    }
  }

  kdq_finalize(toxbot_msgs, &chat->send_queue);
}

static void toxbot_chats_rm(toxbot_t *bot) {
  khiter_t iter = kh_begin(&bot->chats);

  for (; iter != kh_end(&bot->chats); iter++) {
    if (kh_exist(&bot->chats, iter)) {
      toxbot_chat_t *chat = kh_value(&bot->chats, iter);
      
      toxbot_msgs_rm(chat);
      del(chat);
    }
  }

  kh_finalize(toxbot_chats, &bot->chats);
}

void toxbot_chat_send(toxbot_chat_t *chat,
                      TOX_MESSAGE_TYPE type,
                      const char *fmt, ...) {
  va_list ap;
  char *sp;
  int sl;
  
  va_start(ap, fmt);
  sl = vasprintf(&sp, fmt, ap);
  va_end(ap);

  dbg("send `%s` to %u", sp, chat->friend_number);
  
  toxbot_msg_t *msg = kdq_pushp(toxbot_msgs, &chat->send_queue);
  msg->type = type;
  msg->message = sp;
  msg->length = sl;
  msg->message_id = 0;
  
  toxbot_chat_resume(chat);
}

static void
log_cb(Tox *tox,
       TOX_LOG_LEVEL level,
       const char *file,
       uint32_t line,
       const char *func,
       const char *message,
       void *user_data) {
  toxbot_t *bot = user_data;
  
  (void)tox;
  (void)level;
  (void)bot;
  
  fprintf(stderr, "[%s:%u %s()] %s\n", file, line, func, message);
}

#define plugins_invoke(bot, method, ...) {     \
    khint_t i = kh_begin(&(bot)->plugins);     \
    for (; i < kh_end(&(bot)->plugins); i++) { \
      if (kh_exist(&(bot)->plugins, i)) {      \
        toxbot_plugin_t *plugin =              \
          &kh_value(&(bot)->plugins, i);       \
        if (plugin->vmt->method) {             \
          dbg("invoke %s." #method,            \
              plugin->name);                   \
          plugin->vmt->method(__VA_ARGS__);    \
        }                                      \
      }                                        \
    }                                          \
  }

static void
self_connection_status_cb(Tox *tox,
                          TOX_CONNECTION connection_status,
                          void *user_data) {
  toxbot_t *bot = user_data;

  if (connection_status != TOX_CONNECTION_NONE) {
    /* try resend queued messages */
    khiter_t iter = kh_begin(&bot->chats);

    for(; iter < kh_end(&bot->chats); iter++) {
      if (kh_exist(&bot->chats, iter)) {
        toxbot_chat_t *chat = kh_value(&bot->chats, iter);
        dbg("resend chat for %u", chat->friend_number);
        toxbot_chat_resume(chat);
      }
    }
  }
  
  plugins_invoke(bot, self_connection_status,
                 tox, connection_status,
                 plugin);
  
  toxbot_profile_save(bot);
}

static void
friend_connection_status_cb(Tox *tox,
                            uint32_t friend_number,
                            TOX_CONNECTION connection_status,
                            void *user_data) {
  toxbot_t *bot = user_data;

  if (connection_status == TOX_CONNECTION_NONE) {
    /* close chat if no queued messages */
    toxbot_chat_close_empty(bot, friend_number);
  } else {
    /* try resend queued messages */
    toxbot_chat_t *chat = toxbot_chat_find(bot, friend_number);
    if (chat) {
      dbg("resend chat for %u", friend_number);
      toxbot_chat_resume(chat);
    }
  }
  
  plugins_invoke(bot, friend_connection_status,
                 tox, friend_number, connection_status,
                 plugin);
  
  toxbot_profile_save(bot);
}

static void
friend_name_cb(Tox *tox,
               uint32_t friend_number,
               const uint8_t *name,
               size_t length,
               void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_name_cb `%s`", name);
  
  plugins_invoke(bot, friend_name,
                 tox, friend_number, name, length,
                 plugin);

  toxbot_profile_save(bot);
}

static void
friend_status_cb(Tox *tox,
                 uint32_t friend_number,
                 TOX_USER_STATUS status,
                 void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_status_cb %u", status);
  
  plugins_invoke(bot, friend_status,
                 tox, friend_number, status,
                 plugin);
}

static void
friend_status_message_cb(Tox *tox,
                         uint32_t friend_number,
                         const uint8_t *message,
                         size_t length,
                         void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_status_message_cb `%s`", message);

  plugins_invoke(bot, friend_status_message,
                 tox, friend_number, message, length,
                 plugin);
}

static void
friend_typing_cb(Tox *tox,
                 uint32_t friend_number,
                 bool is_typing,
                 void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_typing_cb %u %u", friend_number, is_typing);

  plugins_invoke(bot, friend_typing,
                 tox, friend_number, is_typing,
                 plugin);
}

static void
friend_read_receipt_cb(Tox *tox,
                       uint32_t friend_number,
                       uint32_t message_id,
                       void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_read_receipt_cb %u from %u", message_id, friend_number);
  toxbot_chat_receipt(bot, friend_number, message_id);
  
  plugins_invoke(bot, friend_read_receipt,
                 tox, friend_number, message_id,
                 plugin);
}

static void
friend_request_cb(Tox *tox,
                  const uint8_t *public_key,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  toxbot_t *bot = user_data;
  
  plugins_invoke(bot, friend_request,
                 tox, public_key, message, length,
                 plugin);

  toxbot_profile_save(bot);
}

static void
friend_message_cb(Tox *tox,
                  uint32_t friend_number,
                  TOX_MESSAGE_TYPE type,
                  const uint8_t *message,
                  size_t length,
                  void *user_data) {
  toxbot_t *bot = user_data;
  
  dbg("friend_message_cb `%s` from %u", message, friend_number);

  plugins_invoke(bot, friend_message,
                 tox, friend_number, type, message, length,
                 plugin);
}

static void
friend_lossy_packet_cb(Tox *tox,
                       uint32_t friend_number,
                       const uint8_t *data,
                       size_t length,
                       void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_lossy_packet_cb from %u", friend_number);
  
  plugins_invoke(bot, friend_lossy_packet,
                 tox, friend_number, data, length,
                 plugin);
}

static void
friend_lossless_packet_cb(Tox *tox,
                          uint32_t friend_number,
                          const uint8_t *data,
                          size_t length,
                          void *user_data) {
  toxbot_t *bot = user_data;

  dbg("friend_lossless_packet_cb from %u", friend_number);

  plugins_invoke(bot, friend_lossless_packet,
                 tox, friend_number, data, length,
                 plugin);
}

static void
file_recv_control_cb(Tox *tox,
                     uint32_t friend_number,
                     uint32_t file_number,
                     TOX_FILE_CONTROL control,
                     void *user_data) {
  toxbot_t *bot = user_data;

  plugins_invoke(bot, file_recv_control,
                 tox, friend_number, file_number, control,
                 plugin);
}

static void
file_chunk_request_cb(Tox *tox,
                      uint32_t friend_number,
                      uint32_t file_number,
                      uint64_t position,
                      size_t length,
                      void *user_data) {
  toxbot_t *bot = user_data;

  plugins_invoke(bot, file_chunk_request,
                 tox, friend_number, file_number, position, length,
                 plugin);
}

static void
file_recv_cb(Tox *tox,
             uint32_t friend_number,
             uint32_t file_number,
             uint32_t kind,
             uint64_t file_size,
             const uint8_t *filename,
             size_t filename_length,
             void *user_data) {
  toxbot_t *bot = user_data;

  plugins_invoke(bot, file_recv,
                 tox, friend_number, file_number, kind, file_size, filename, filename_length,
                 plugin);
}

static void
file_recv_chunk_cb(Tox *tox,
                   uint32_t friend_number,
                   uint32_t file_number,
                   uint64_t position,
                   const uint8_t *data,
                   size_t length,
                   void *user_data) {
  toxbot_t *bot = user_data;

  plugins_invoke(bot, file_recv_chunk,
                 tox, friend_number, file_number, position, data, length,
                 plugin);
}

static void
conference_invite_cb(Tox *tox,
                     uint32_t friend_number,
                     TOX_CONFERENCE_TYPE type,
                     const uint8_t *cookie,
                     size_t length,
                     void *user_data) {
  toxbot_t *bot = user_data;
  
  plugins_invoke(bot, conference_invite,
                 tox, friend_number, type, cookie, length,
                 plugin);

  toxbot_profile_save(bot);
}

static void
conference_title_cb(Tox *tox,
                    uint32_t conference_number,
                    uint32_t peer_number,
                    const uint8_t *title,
                    size_t length,
                    void *user_data) {
  toxbot_t *bot = user_data;
  
  plugins_invoke(bot, conference_title,
                 tox, conference_number, peer_number, title, length,
                 plugin);
}

static void
conference_message_cb(Tox *tox,
                      uint32_t conference_number,
                      uint32_t peer_number,
                      TOX_MESSAGE_TYPE type,
                      const uint8_t *message,
                      size_t length,
                      void *user_data) {
  toxbot_t *bot = user_data;
  
  plugins_invoke(bot, conference_message,
                 tox, conference_number, peer_number, type, message, length,
                 plugin);
}

static void
conference_namelist_change_cb(Tox *tox,
                              uint32_t conference_number,
                              uint32_t peer_number,
                              TOX_CONFERENCE_STATE_CHANGE change,
                              void *user_data) {
  toxbot_t *bot = user_data;
  
  plugins_invoke(bot, conference_namelist_change,
                 tox, conference_number, peer_number, change,
                 plugin);
}

static int toxbot_init(toxbot_t *bot) {
  bool has_profile = false;
  
  {
    struct Tox_Options *opts = tox_options_new(NULL);
    
    if (!opts) {
      return 0;
    }
    
    tox_options_set_udp_enabled(opts, bot->use_udp);
    tox_options_set_ipv6_enabled(opts, bot->use_ipv6);
    
    if (toxbot_profile_load(bot, opts)) {
      has_profile = true;
    }
    
    TOX_ERR_NEW err_new;
    bot->tox = tox_new(opts, &err_new);

    if (has_profile) {
      del((uint8_t*)tox_options_get_savedata_data(opts));
    }
    tox_options_free(opts);
    
    if (!bot->tox) {
      return 0;
    }
  }
  
  tox_callback_log(bot->tox, log_cb, NULL);

  /* set self callbacks */
  tox_callback_self_connection_status(bot->tox, self_connection_status_cb);

  /* set friend callbacks */
  tox_callback_friend_connection_status(bot->tox, friend_connection_status_cb);
  tox_callback_friend_name(bot->tox, friend_name_cb);
  tox_callback_friend_status(bot->tox, friend_status_cb);
  tox_callback_friend_status_message(bot->tox, friend_status_message_cb);
  tox_callback_friend_typing(bot->tox, friend_typing_cb);
  tox_callback_friend_read_receipt(bot->tox, friend_read_receipt_cb);
  tox_callback_friend_request(bot->tox, friend_request_cb);
  tox_callback_friend_message(bot->tox, friend_message_cb);
  tox_callback_friend_lossy_packet(bot->tox, friend_lossy_packet_cb);
  tox_callback_friend_lossless_packet(bot->tox, friend_lossless_packet_cb);

  /* set file callbacks */
  tox_callback_file_recv_control(bot->tox, file_recv_control_cb);
  tox_callback_file_chunk_request(bot->tox, file_chunk_request_cb);
  tox_callback_file_recv(bot->tox, file_recv_cb);
  tox_callback_file_recv_chunk(bot->tox, file_recv_chunk_cb);

  /* set conference callbacks */
  tox_callback_conference_invite(bot->tox, conference_invite_cb);
  tox_callback_conference_message(bot->tox, conference_message_cb);
  tox_callback_conference_title(bot->tox, conference_title_cb);
  tox_callback_conference_namelist_change(bot->tox, conference_namelist_change_cb);
  
  dbg("bootstrap");
  toxbot_bootstrap(bot);

  dbg("identify");
  if (bot->self_name && !has_profile) {
    tox_self_set_name(bot->tox, bot->self_name, strlen(bot->self_name), NULL);
  }
  
  tox_self_set_status(bot->tox, TOX_USER_STATUS_NONE);
  tox_self_set_status_message(bot->tox, "Hi", sizeof("Hi")-1, NULL);
  
  toxbot_profile_save(bot);
  
  {
    uint8_t pubkey[TOX_PUBLIC_KEY_SIZE];
    tox_self_get_public_key(bot->tox, pubkey);
    char hexkey[TOX_PUBLIC_KEY_SIZE*2+1];
    bin2hex(hexkey, pubkey, sizeof(pubkey));
    hexkey[TOX_PUBLIC_KEY_SIZE*2] = '\0';
    dbg("pubkey: %s", hexkey);
  }
  
  {
    uint8_t address[TOX_ADDRESS_SIZE];
    tox_self_get_address(bot->tox, address);
    char hexstr[TOX_ADDRESS_SIZE*2+1];
    bin2hex(hexstr, address, sizeof(address));
    hexstr[TOX_ADDRESS_SIZE*2] = '\0';
    dbg("address: %s", hexstr);
  }
  
  return 1;
}

static void toxbot_done(toxbot_t *bot) {
  tox_kill(bot->tox);
}

static void toxbot_iter_stop(toxbot_t *bot) {
  uv_timer_stop(&bot->iter_timer);
}

static void interrupt_signal_cb(uv_signal_t* handle,
                                int signum) {
  (void)signum;
  toxbot_t *bot = handle->data;
  
  uv_signal_stop(&bot->int_signal);
  
  bot->alive = false;
  
  plugins_invoke(bot, plugin_stop,
                 bot->tox,
                 plugin);
}

static void toxbot_iter_timer_cb(uv_timer_t *handle) {
  toxbot_t *bot = handle->data;
  
  tox_iterate(bot->tox, bot);
  
  uint32_t timeout = tox_iteration_interval(bot->tox);

  if (bot->alive || uv_loop_alive(bot->loop)) {
    uv_timer_start(&bot->iter_timer,
                   toxbot_iter_timer_cb,
                   timeout, 0);
  }
}

static void toxbot_iter_start(toxbot_t *bot) {
  uv_signal_start(&bot->int_signal,
                  interrupt_signal_cb,
                  SIGINT);
  
  uv_timer_start(&bot->iter_timer,
                 toxbot_iter_timer_cb,
                 tox_iteration_interval(bot->tox),
                 0);
}

static int toxbot_run(toxbot_t *bot, int argc, char *argv[]) {
  if (!toxbot_getopt(bot, argc, argv)) {
    return 1;
  }
  
  toxbot_plugins_init(bot);
  
  if (bot->lang) {
    l10n_use(bot->lang);
    plugins_invoke(bot, plugin_use_lang,
                   bot->lang,
                   plugin);
  }

  if (bot->plugin_info) {
    toxbot_plugins_info(bot);
    goto done;
  }
  
  if (!toxbot_init(bot)) {
    return 1;
  }

  bot->alive = true;
  
  plugins_invoke(bot, plugin_start,
                 bot->tox,
                 plugin);
  
  toxbot_iter_start(bot);
  
  dbg("loop start");
  uv_run(bot->loop, UV_RUN_DEFAULT);
  dbg("loop stop");

  toxbot_iter_stop(bot);

  toxbot_done(bot);
  
 done:
  toxbot_plugins_done(bot);

  uv_loop_close(bot->loop);

  return 0;
}

KDQ_IMPL(toxbot_msgs, klib_global, uint32_t, toxbot_msg_t);

#if USE_DEBUG
KMAP_IMPL(toxbot_chats, klib_global, khint32_t, toxbot_chat_t*, kh_int32_hash);
#else
KMAP_IMPL(toxbot_int_voidp, klib_global, khint32_t, void*, kh_int32_hash);
KDQ_IMPL(toxbot_voidp, klib_global, uint32_t, void*);
#endif

KMAP_IMPL(toxbot_plugins, klib_global, kh_cstr_t, toxbot_plugin_t, kh_str_hash);

int main(int argc, char *argv[]) {
  toxbot_t bot;

  bot.plugin_info = false;
  bot.self_name = NULL;
  bot.profile_path = TOXBOT_PROFILE_PATH_DEFAULT;
  bot.use_udp = true;
  bot.use_ipv6 = true;
  bot.lang = NULL;
  bot.tox = NULL;
  
#if TOXBOT_PLUGIN_LOADABLE
  bot.plugin_paths = NULL;
#endif

  bot.loop = uv_default_loop();

  kh_prepare(c_strings, &bot.whitelist);
  kh_prepare(c_strings, &bot.blacklist);

  {
    const char *str = getenv(TOXBOT_PLUGIN_WHITELIST_ENV_VAR);
    if (str) {
      parse_namelist(&bot.whitelist, str);
    }
    str = getenv(TOXBOT_PLUGIN_BLACKLIST_ENV_VAR);
    if (str) {
      parse_namelist(&bot.blacklist, str);
    }
  }
  
  kh_prepare(toxbot_plugins, &bot.plugins);
  kh_prepare(toxbot_chats, &bot.chats);
  
  uv_timer_init(bot.loop, &bot.iter_timer);
  bot.iter_timer.data = &bot;

  uv_signal_init(bot.loop, &bot.int_signal);
  bot.int_signal.data = &bot;
  
  int res = toxbot_run(&bot, argc, argv);

  toxbot_chats_rm(&bot);

  kh_finalize(toxbot_plugins, &bot.plugins);

  return res;
}
