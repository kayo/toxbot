# Compiler
#COMPILER_NAME ?= arm-none-eabi-
#OBJECT_ARCH ?= elf32-littlearm arm
#GDBREMOTE ?= localhost:3333

# Base path to build root
BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

MKDIR := $(BASEPATH)rules
INCDIR := $(BASEPATH)include
SRCDIR := $(BASEPATH)src
LIBSDIR := $(BASEPATH)libs

-include config.mk

include $(MKDIR)/macro.mk
include $(MKDIR)/build.mk
include $(MKDIR)/stalin.mk
include $(MKDIR)/pkgconfig.mk
include $(MKDIR)/option.mk

include $(LIBSDIR)/l10n/l10n.mk

TARGET.PKGS += libtoxcore libtoxav libsodium
TARGET.PKGS += libuv

include $(LIBSDIR)/klib/klib.mk

libelastr.BASEPATH := $(LIBSDIR)/elastr
include $(MKDIR)/elastr.mk

TARGET.LIBS += libtoxbot_base
libtoxbot_base.INHERIT := stalin libtoxcore libsodium libuv libk libelastr
libtoxbot_base.CDIRS := $(INCDIR)

TARGET.OPTS += libtoxbot_base
libtoxbot_base.OPTS := $(SRCDIR)/toxbot.cf

TARGET.LIBS += libtoxbot
libtoxbot.INHERIT := libtoxbot_base
libtoxbot.SRCS := $(addprefix $(SRCDIR)/,\
  getopt.c \
  bootstrap.c \
  profile.c \
  plugin.c \
  utils.c \
  toxbot.c)

$(call ADDRULES,\
OPT_RULES:TARGET.OPTS)

libtoxbot.LANGS := $(program.languages)

plugin.list.found := $(patsubst %/plugin.mk,%,$(wildcard $(addsuffix /*/plugin.mk,$(plugin.path.source))))
plugin.list.available := $(notdir $(plugin.list.found))
plugin.list.enabled := $(filter-out $(plugin.list.black),$(if $(plugin.list.white),$(filter $(plugin.list.white),$(plugin.list.available)),$(plugin.list.available)))
plugin.list.internal := $(filter $(plugin.list.embed),$(plugin.list.enabled))
plugin.list.external := $(filter-out $(plugin.list.embed),$(plugin.list.enabled))

# <plugin-name> <plugin-path>
define PLUGIN_RULES
TARGET.LIBS += $$(if $$(filter $(1),$$(plugin.list.enabled)),libtoxbot/$(1))
libtoxbot/$(1).BASEDIR := $(2)
libtoxbot/$(1).INHERIT := libtoxbot_base
libtoxbot/$(1).LANGS := $(program.languages)
libtoxbot/$(1).CDEFS := TOXBOT_PLUGIN_NAME='$(1)'
TARGET.DYNS += $$(if $$(filter $(1),$$(plugin.list.external)),toxbot/$(1))
toxbot/$(1).INHERIT := libtoxbot/$(1)
toxbot/$(1).DEPLIBS* := libtoxbot/$(1)
include $(2)/plugin.mk
TARGET.OPTS += libtoxbot/$(1)
endef

$(foreach p,\
$(patsubst %/plugin.mk,%,\
$(wildcard $(addsuffix /*/plugin.mk,\
$(plugin.path.source)))),\
$(eval $(call PLUGIN_RULES,$(notdir $(p)),$(p))))

$(call ADDRULES,\
OPT_RULES:TARGET.OPTS)

TARGET.BINS += toxbot
toxbot.INHERIT := libtoxbot
toxbot.DEPLIBS* := libtoxbot libelastr
toxbot.DEPLIBS* += $(addprefix libtoxbot/,$(plugin.list.internal))
toxbot.LDFLAGS += $(if $(call option-true,$(plugin.loadable)),-rdynamic)
toxbot.ENVIRON ?= TOXBOT_PLUGIN_PATH=bin/toxbot
toxbot.CMDLINE ?= -l ru

# Provide rules
$(call ADDRULES,\
PKG_RULES:TARGET.PKGS\
LOC_RULES:TARGET.LIBS\
LIB_RULES:TARGET.LIBS\
DYN_RULES:TARGET.DYNS\
BIN_RULES:TARGET.BINS)
