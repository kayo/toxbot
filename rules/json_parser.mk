#libjson_parser.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libjson_parser
libjson_parser.INHERIT := stalin
libjson_parser.CDIRS := $(libjson_parser.BASEPATH)/include
libjson_parser.SRCS := $(addprefix $(libjson_parser.BASEPATH)/src/,\
  tokenizer.c)
